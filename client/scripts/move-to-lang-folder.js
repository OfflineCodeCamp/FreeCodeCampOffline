const path = require("path");
const fs = require("fs");

let lang = process.env["APP_LANG"];
if (!lang) {
    console.warn("APP_LANG is not set. Default value: en");
    lang = "en";
}

fs.rmdirSync(path.resolve(`../electron/langs/${lang}`), { recursive: true });
fs.renameSync(
    path.resolve("./public"),
    path.resolve(`../electron/langs/${lang}`)
);
fs.writeFileSync(
    path.resolve("../electron/electron-builder.env"),
    `APP_LANG=${lang}`,
    "utf-8"
);

const path = require("path");
const fs = require("fs");

const { curriculumBlackList } = require("../../config/course-settings.json");

curriculumBlackList.forEach((courseName) => {
    fs.rmdirSync(path.resolve(`./public/learn/${courseName}`), { recursive: true });
    fs.rmdirSync(path.resolve(`./public/page-data/learn/${courseName}`), { recursive: true });
});

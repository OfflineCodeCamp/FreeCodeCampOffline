const fs = require("fs");
const argv = (() => {
    const arguments = {};
    process.argv.slice(2).map( (element) => {
        const matches = element.match( '--([a-zA-Z0-9]+)=(.*)');
        if ( matches ){
            arguments[matches[1]] = matches[2]
                .replace(/^['"]/, '').replace(/['"]$/, '');
        }
    });
    return arguments;
})();

const langs = {
    en:"english",
    es:"espanol",
    zh:"chinese",
    "zh-Hant":"chinese-traditional",
    it:"italian",
    "pt-BR":"portuguese",
}

const currentLang = langs[argv.lang]

const configName = './electron/config.json';
const config = JSON.parse(fs.readFileSync(configName).toString());
config.clientLocale = currentLang
config.curriculumLocale = currentLang
fs.writeFileSync(configName, JSON.stringify(config));
fs.writeFileSync("electron-builder.env", `APP_LANG=${argv.lang}`);
console.log(`Language changed. Language: ${currentLang}`)
const { runExec } = require("./utilities");

const removeCommand = "cd ./node_modules && rm -r -f sharp";
const installCommands = "cd .. && npm install"
const runCommand = `${removeCommand} && ${installCommands}`;
runExec(runCommand);

const path = require('path');
const fs = require('fs');

fs.rmdirSync(path.resolve('./public'), { recursive: true });
fs.rmdirSync(path.resolve('./public_dev'), { recursive: true });
fs.mkdir(path.resolve('./public'), (err) => {
    if (err) {
        return console.error(err);
    }
    console.log('Public directory created successfully!');
});

import { Router } from '@reach/router';
// import { withPrefix } from 'gatsby';
import React from 'react';

/* eslint-disable max-len */
import FourOhFour from '../components/FourOhFour';
/* eslint-enable max-len */

function FourOhFourPage(): JSX.Element {
  return (
    <Router>
      {/* Error from installing @types/react-helmet and @types/react-redux */}
      {/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */}
      {/* @ts-ignore */}
      {/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */}
      {/* @ts-ignore */}
      <FourOhFour default={true} />
    </Router>
  );
}

FourOhFourPage.displayName = 'FourOhFourPage';

export default FourOhFourPage;

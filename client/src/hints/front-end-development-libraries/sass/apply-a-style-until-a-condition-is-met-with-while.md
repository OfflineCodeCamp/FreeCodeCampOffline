﻿# [Apply a Style Until a Condition is Met with @while](https://www.freecodecamp.org/learn/front-end-development-libraries/sass/apply-a-style-until-a-condition-is-met-with-while)

---
## Problem Explanation

This program is very simple, the trick is to remember how while looping works.


---
## Hints

### Hint 1

*   **Make sure your zoom settings are at `100%` or `default` otherwise tests sometimes fail. **

### Hint 2

*   You will initialise the loop first with x as: `$x: 1`


### Hint 3

*   See the example for `@while` syntax, `@while $x < 11`


### Hint 4

*   to set class properties inside a loop we reference them enclosed by #{}, hence ere it will become: `.text-#{$x}`


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```scss
$x: 1;
@while $x < 11 {
  .text-#{$x} { 

﻿# [Use @each to Map Over Items in a List](https://www.freecodecamp.org/learn/front-end-development-libraries/sass/use-each-to-map-over-items-in-a-list)

---
## Problem Explanation
The @each directive loops over a list and for each iteration the variable is assigned the value in the list.

**Example:**

```html
<style type='text/sass'>
  
@each $color in white, black, blue {
  .#{$color}-font {
    color: $color;
  }
}

</style>

<div class="white-font"></div>
<div class="black-font"></div>

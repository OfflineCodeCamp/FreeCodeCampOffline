﻿# [Create Reusable CSS with Mixins](https://www.freecodecamp.org/learn/front-end-development-libraries/sass/create-reusable-css-with-mixins)

---
## Problem Explanation

`Mixin` is one of the great features that let developers use `SASS` instead of plain `CSS`, as it allows you to have a `Function` inside your Stylesheet!

To create a mixin you should follow the following scheme:

```scss
@mixin custom-mixin-name($param1, $param2, ....) {
    // CSS Properties Here...
}
```
And to use it in your element(s), you should use `@include` followed by your `Mixin` name, as following:
```scss
element {
    @include custom-mixin-name(value1, value2, ....);
}
```

#### Relevant Links


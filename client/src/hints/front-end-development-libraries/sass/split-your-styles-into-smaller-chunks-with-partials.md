﻿# [Split Your Styles into Smaller Chunks with Partials](https://www.freecodecamp.org/learn/front-end-development-libraries/sass/split-your-styles-into-smaller-chunks-with-partials)


---
## Hints 

### Hint 1

Use `@import`


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

Use `@import` to import `_variables.scss` like:

```sass
@import 'variables'
```
</details>
﻿# [Map State to Props](https://www.freecodecamp.org/learn/front-end-development-libraries/react-and-redux/map-state-to-props)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```jsx
const state = [];

// change code below this line
const mapStateToProps = (state)=>{
  return {
    messages: state
  }
}
```
</details>
﻿# [Map Dispatch to Props](https://www.freecodecamp.org/learn/front-end-development-libraries/react-and-redux/map-dispatch-to-props)


---
## Solutions
  
<details><summary>Solution 1 (Click to Show/Hide)</summary>

```jsx
const addMessage = (message) => {
  return {
    type: 'ADD',
    message: message
  }
};

// change code below this line
const mapDispatchToProps = (dispatch) => {
    return {
        submitNewMessage: (message)=>{
            dispatch(addMessage(message))
        }
    }
}
```
</details>
﻿# [Add Font Awesome Icons to our Buttons](https://www.freecodecamp.org/learn/front-end-development-libraries/bootstrap/add-font-awesome-icons-to-our-buttons)

---
## Problem Explanation
The challenge requires you to add a thumbs up icon from font awesome to the button with the text 'Like' in it


---
## Hints

### Hint 1

The ``` <i> ``` tag is not self-closing.
 
### Hint 2


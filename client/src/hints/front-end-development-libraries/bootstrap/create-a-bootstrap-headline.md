﻿# [Create a Bootstrap Headline](https://www.freecodecamp.org/learn/front-end-development-libraries/bootstrap/create-a-bootstrap-headline)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<h3 class="text-primary text-center">jQuery Playground</h3>
```

</details>
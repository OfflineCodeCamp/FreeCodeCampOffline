﻿# [House our page within a Bootstrap container-fluid div](https://www.freecodecamp.org/learn/front-end-development-libraries/bootstrap/house-our-page-within-a-bootstrap-container-fluid-div)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<div class="container-fluid">
    <h3 class="text-primary text-center">jQuery Playground</h3>
</div>
```

</details>
﻿# [Split Your Bootstrap Row](https://www.freecodecamp.org/learn/front-end-development-libraries/bootstrap/split-your-bootstrap-row)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<div class="container-fluid">
  <h3 class="text-primary text-center">jQuery Playground</h3>
  <div class="row">
    <div class="col-xs-6"></div>
    <div class="col-xs-6"></div>
  </div>
</div>
```
</details>
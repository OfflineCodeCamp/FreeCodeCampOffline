﻿# [Use Array.filter() to Dynamically Filter an Array](https://www.freecodecamp.org/learn/front-end-development-libraries/react/use-array-filter-to-dynamically-filter-an-array)


---
## Hints

### Hint 1

Use ```.filter()``` to create a new array that only shows users online.


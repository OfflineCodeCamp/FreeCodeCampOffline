﻿# [Render Conditionally from Props](https://www.freecodecamp.org/learn/front-end-development-libraries/react/render-conditionally-from-props)


---
## Hints

### Hint 1
Use `Math.random()` with the ternary operator to return `true` or `false`.

### Hint 2
Render the `Results` component within the `GameOfChance` component. Pass the variable `expression` to `Results` as a prop called `fiftyFifty`.

### Hint 3
Correct the `counter` property of the `state` so that the `handleClick()` method increments it by 1.

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

In `render()` method use `Math.random()` as mentioned in the challenge description and write a ternary expression to return `true` if `Math.random()` returns a value `>= 0.5`, and `false` otherwise.
```jsx

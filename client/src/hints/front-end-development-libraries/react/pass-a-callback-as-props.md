﻿# [Pass a Callback as Props](https://www.freecodecamp.org/learn/front-end-development-libraries/react/pass-a-callback-as-props)

---
## Problem Explanation
- Add the `GetInput` component to the render method in MyApp, then pass it a prop called `inpu`t assigned to `inputValue` from MyApp's state. Also create a prop called `handleChange` and pass the input handler `handleChange` to it.
- Add `RenderInput` to the render method in MyApp, then create a prop called `input` and pass the `inputValue` from state to it.


---
## Hints

### Hint 1
`state` is a property of `Myapp` class, so use 'this.state' to get the object value

### Hint 2
To learn more about state and props, read [State and Lifecycle](https://reactjs.org/docs/state-and-lifecycle.html) and [Components and Props](https://reactjs.org/docs/components-and-props.html).


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
class MyApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

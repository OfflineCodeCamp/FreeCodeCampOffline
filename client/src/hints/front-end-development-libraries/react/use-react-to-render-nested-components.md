﻿# [Use React to Render Nested Components](https://www.freecodecamp.org/learn/front-end-development-libraries/react/use-react-to-render-nested-components)

---
## Problem Explanation

You have learned in earlier lessons, that there are two ways to create a React component:

1. Stateless functional component - using a JavaScript function.

2. Define a React component using the ES6 syntax.


---
## Hints

### Hint 1
In this challenge, we have defined two stateless functional components, i.e. using JavaScript functions. Recall, once a component has been created, it can be rendered in the same way as an HTML tag, by using the component name inside HTML opening and closing brackets. For example, to nest a component called Dog inside a parent component called Animals, you'd simply return the Dog component from the Animals component like this:

```javascript
return <Dog />;
```
Try this with the TypesOfFruit and Fruits components.

---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
const TypesOfFruit = () => {
  return (
    <div>
      <h2>Fruits:</h2>
      <ul>
        <li>Apples</li>
        <li>Blueberries</li>
        <li>Strawberries</li>
        <li>Bananas</li>
      </ul>
    </div>
  );
};

const Fruits = () => {
  return (
    <div>
      { /* change code below this line */ }
      <TypesOfFruit/>
      { /* change code above this line */ }
    </div>
  );
};

class TypesOfFood extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h1>Types of Food:</h1>
        { /* change code below this line */ }
          <Fruits/>
        { /* change code above this line */ }
      </div>
    );
  }
};
```

#### Code Explanation

- Code explanation goes here

#### Relevant Links

- [Link Text](link_url_goes_here)

</details>
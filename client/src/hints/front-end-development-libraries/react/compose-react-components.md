﻿# [Compose React Components](https://www.freecodecamp.org/learn/front-end-development-libraries/react/compose-react-components)


---
## Hints

### Hint 1 

Use nested components as in the previous challenge to render components.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

The following is the solution to the challenge, where it render `Citrus` and `NonCitrus` in a component which is then rendered in another:

```jsx
class Fruits extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <h2>Fruits:</h2>
        <NonCitrus />
        <Citrus />
      </div>
    );
  }
};

class TypesOfFood extends React.Component {
  constructor(props) {
     super(props);
  }
  render() {
    return (
      <div>

﻿# [Use State to Toggle an Element](https://www.freecodecamp.org/learn/front-end-development-libraries/react/use-state-to-toggle-an-element)

---
## Problem Explanation
You can toggle an element by checking and changing its state.


---
## Hints

### Hint 1

- Remember to bind ```this``` to the method constructor.

```javascript
this.toggleVisibility = this.toggleVisibility.bind(this);
```

### Hint 2

- Remember, you can use a JavaScript function to check for the state of an element.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```jsx
class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibility: false
    };
    // change code below this line
    this.toggleVisibility = this.toggleVisibility.bind(this);
    // change code above this line
  }
  // change code below this line
  toggleVisibility() {
    this.setState(state => {
      if (state.visibility === true) {
         return { visibility: false };
       } else {
         return { visibility: true };
      }
    });
  }
  // change code above this line
  render() {
    if (this.state.visibility) {
      return (
        <div>
          <button onClick={this.toggleVisibility}>Click Me</button>
          <h1>Now you see me!</h1>
        </div>
      );
    } else {
      return (
        <div>
          <button onClick={this.toggleVisibility}>Click Me</button>
        </div>
      );
    }
  }
};
```

</details>

<details><summary>Solution 2 (Click to Show/Hide)</summary>

```jsx
class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibility: false
    };
    this.toggleVisibility = this.toggleVisibility.bind(this);
   }
  toggleVisibility() {
    this.setState(state => ({
      visibility: !state.visibility
    }));
  }
  render() {
    if (this.state.visibility) {
      return (
        <div>
          <button onClick = {this.toggleVisibility}>Click Me</button>
          <h1>Now you see me!</h1>
        </div>
      );
    } else {
      return (
        <div>
          <button onClick = {this.toggleVisibility}>Click Me</button>
        </div>
      );
    }
  }
};
```

</details>
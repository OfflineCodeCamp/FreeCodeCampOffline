﻿# [Create a Controlled Form](https://www.freecodecamp.org/learn/front-end-development-libraries/react/create-a-controlled-form)

---
## Problem Explanation
Creating a controlled form is the same process as creating a controlled input, except you need to handle a submit event.


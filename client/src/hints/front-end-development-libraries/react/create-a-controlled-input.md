﻿# [Create a Controlled Input](https://www.freecodecamp.org/learn/front-end-development-libraries/react/create-a-controlled-input)

---
## Problem Explanation
Here the idea is to create a controlled input where the text updates from your state, not from the browser.

So to begin with we have a skeleton in which we have a class named `ControlledInput` and a state variable named `input`. Now all you need to do is take that state and when a change in the input box is observed trigger a function that changes the value inside that input box and in the paragraph below it.

So you first step will be making a function that changes the state variable `input`.
```jsx

handleChange(event) {
    this.setState({
      input: event.target.value
    });
}
```
Remember to bind `this` to the function.
``` jsx
this.handeChange = this.handleChange.bind(this)
```

Now your next step will involve creating an input box and trigger it when someone types anything. Luckily we have an event called `onChange()` to serve this purpose. <br>

But this just won't serve your purpose. Although you might feel that its working. So what's happening here is text updates from the browser not the state. So to correct this we'll add a `value` attribute and set it to `this.state.input` to the input element which will make the input get controlled by state.

```jsx
<input value = {this.state.input} onChange = {this.handleChange}/>
```

It can be a bit hard to digest but to make things further clear try removing the entire `onChange` thing so your code looks like this
```jsx

<input value = {this.state.input}/>
```
Now run the tests again are you able to type anything? <br>
The answer to it will be "NO" since your input box is getting value from the state variable `input` since there is no change in the state `input`(an empty string initially) which will only happen when you trigger the function `handleChange()` which will only happen when you have an event handler like `onChange()` hence the string inside the input box will remain as it is i.e, an empty string.

---

## Solution

[details="Click to show/hide"]
``` react
class ControlledInput extends React.Component {

  constructor(props) {

    super(props);

    this.state = {

      input: ''

    };

    // Change code below this line

    this.handleChange = this.handleChange.bind(this);

    // Change code above this line

  }

  // Change code below this line

  handleChange (event) {

    this.setState({

      input: event.target.value

    })

  }

  // Change code above this line

  render() {

    return (

      <div>

        { /* Change code below this line */}

        <input value={this.state.input} onChange={this.handleChange} />

        { /* Change code above this line */}

        <h4>Controlled Input:</h4>

        <p>{this.state.input}</p>

      </div>

    );

  }

};
```
[/details]
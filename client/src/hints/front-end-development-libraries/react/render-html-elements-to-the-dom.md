﻿# [Render HTML Elements to the DOM](https://www.freecodecamp.org/learn/front-end-development-libraries/react/render-html-elements-to-the-dom)

---
## Problem Explanation
To render an element to the DOM, we use the following syntax
```
ReactDOM.render(<item to be rendered>, <where to be rendered>);
```

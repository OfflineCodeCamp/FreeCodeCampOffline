﻿# [Render React on the Server with renderToString](https://www.freecodecamp.org/learn/front-end-development-libraries/react/render-react-on-the-server-with-rendertostring)


---
## Solutions

You pass a ```class``` to ```.renderToString()``` just like you would pass a component to a ```render``` method.

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```jsx

class App extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return <div/>
  }
};

// change code below this line
ReactDOMServer.renderToString(<App />);
```
</details>
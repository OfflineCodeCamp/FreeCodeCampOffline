﻿# [Use a Ternary Expression for Conditional Rendering](https://www.freecodecamp.org/learn/front-end-development-libraries/react/use-a-ternary-expression-for-conditional-rendering)
This challenge is to use Ternary Expression only instead of using `If/Else` in code,


---
## Hints

### Hint 1
Ternary operator has three parts, but you can combine several ternary expressions together. Here's the basic syntax:
```
condition ? expressionIfTrue : expressionIfFalse
```

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```jsx
const inputStyle = {
  width: 235,
  margin: 5
}

class CheckUserAge extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userAge: '',
      input: ''
    }
    this.submit = this.submit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    this.setState({
      input: e.target.value,
      userAge: ''
    });
  }
  submit() {
    this.setState(state => ({
      userAge: state.input
    }));
  }
  render() {
    const buttonOne = <button onClick={this.submit}>Submit</button>;
    const buttonTwo = <button>You May Enter</button>;
    const buttonThree = <button>You Shall Not Pass</button>;
    return (
      <div>
        <h3>Enter Your Age to Continue</h3>
        <input
          style={inputStyle}
          type="number"
          value={this.state.input}
          onChange={this.handleChange} /><br />
          {
          this.state.userAge === ''
            ? buttonOne
            : this.state.userAge >= 18
              ? buttonTwo
              : buttonThree
          }
      </div>
    );
  }
};
```

</details>
﻿# [Define an Action Creator](https://www.freecodecamp.org/learn/front-end-development-libraries/redux/define-an-action-creator)


---
## Hints

### Hint 1

A function is defined using the following syntax:

```javascript
function functionName() {
  console.log("Do something");
}
```

Where console.log can be changed as per the need.

### Hint 2

A value is returned using the ``` return ``` keyword


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function actionCreator() {
  return action;
}
```
</details>
﻿# [Handle an Action in the Store](https://www.freecodecamp.org/learn/front-end-development-libraries/redux/handle-an-action-in-the-store)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
const defaultState = {
  login: false
};

const reducer = (state = defaultState, action) => {
  // change code below this line
  if (action.type === "LOGIN") {
    return {
      login: true
    };
  } else {
    return state;
  }
  // change code above this line
};

const store = Redux.createStore(reducer);

const loginAction = () => {
  return {
    type: "LOGIN"
  };
};
```
</details>
﻿# [Register a Store Listener](https://www.freecodecamp.org/learn/front-end-development-libraries/redux/register-a-store-listener)

---
## Problem Explanation
Let's break the instructions down to figure exactly what it's asking. 
>*Write a callback function that increments the global variable count every time the store receives an action, and pass this function in to the store.subscribe() method.*

We can summarize these steps into small chunks:
1. write a callback function
2. increment the global variable count
3. pass function to `store.subscribe()` method.

Awesome! Now let's review some of the basics again.

**What is a "callback function" in plain English?**

﻿# [Define a Redux Action](https://www.freecodecamp.org/learn/front-end-development-libraries/redux/define-a-redux-action)


---
## Hints

### Hint 1

Here is how to declare a Redux Action.
```jsx
let action={
  type: 'LOGIN'
}
```
﻿# [Dispatch an Action Event](https://www.freecodecamp.org/learn/front-end-development-libraries/redux/dispatch-an-action-event)

---
## Problem Explanation
Dispatch the LOGIN action to the Redux store by calling the dispatch method, and pass in the action created by `loginAction()`.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```jsx
store.dispatch(loginAction());
```

</details>
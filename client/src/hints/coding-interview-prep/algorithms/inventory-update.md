﻿# [Inventory Update](https://www.freecodecamp.org/learn/coding-interview-prep/algorithms/inventory-update)

---
## Problem Explanation

In this problem, you've to compare and update the inventory stored in a 2D array against a second 2D array of a fresh delivery. Update the current existing inventory item quantities (in `arr1`). If an item cannot be found, add the new item and quantity into the inventory array. The returned inventory array should be in alphabetical order by item.

The current as well as new inventory will be in this format: `[[2, "item-0"], [3, "item-1"], [67, "item-2"], [7, "item-3"]]`.

#### Relevant Links

* <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array' target='_blank' rel='nofollow'>JS Array</a>


---
## Hints

### Hint 1

You need to work through each item of the new inventory to see if it exists in the current inventory or not. Remember that the product name is stored as the second element of each sub-array: `array[0][1] = "item-name"`.


### Hint 2

If the item exists, you need to add the quantity from the new inventory. If the item doesn't exist, you need to add the entire item.


### Hint 3

Return the completed inventory in alphabetical order.

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function updateInventory(arr1, arr2) {
  // Variable for location of product
  var index;

  // A helper method to return the index of a specified product (undefined if not found)
  var getProductIndex = function(name) {
    for (var i = 0; i < this.length; i++) {
      if (this[i][1] === name) {
        return i;
      }
    }
    return undefined;
  };

  // For each item of the new Inventory
  for (var i = 0; i < arr2.length; i++) {
    // Invoke our helper function using arr1 as this
    index = getProductIndex.call(arr1, arr2[i][1]);

    // If the item doesn't exist
    if (index === undefined) {
      // Push the entire item
      arr1.push(arr2[i]);
    } else {
      // Add the new quantity of the current item
      arr1[index][0] += arr2[i][0];
    }
  }

  // Sort alphabetically, by the product name of each item
  arr1.sort(function(a, b) {
    if (a[1] > b[1]) {
      return 1;
    }
    if (a[1] < b[1]) {
      return -1;
    }
    return 0;
  });

  return arr1;
}

// test here
// Example inventory lists
var curInv = [
  [21, "Bowling Ball"],
  [2, "Dirty Sock"],
  [1, "Hair Pin"],
  [5, "Microphone"]
];

var newInv = [
  [2, "Hair Pin"],
  [3, "Half-Eaten Apple"],
  [67, "Bowling Ball"],
  [7, "Toothpaste"]
];

updateInventory(curInv, newInv);
```


#### Code Explanation

* The variable **index** stores the location (index) of a product.
* The helper function `getProductIndex()` returns the index of a specified product. It iterates through each element of the array that it is called on until it can find the name parameter. If the product is not found in the inventory, `undefined` is returned.
* Then, each item in the new inventory (delivery) is worked through:
  * **index** is set to the result of invoking the helper function i.e., search the new inventory for that product name and return its index.
  * If the item is found, quantity of the product is added to the quantity of the same product in current inventory.
  * If the item is not found, the entire product (name and quantity) is added to the current inventory.
* The updated inventory, **arr1**, is then sorted by product name (held in `arr1[x][1]`).
* The final - updated as well as sorted array is then returned.

#### Relevant Links

* <a href='https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Operators/this' target='_blank' rel='nofollow'>JS this</a>
* <a href='https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/length' target='_blank' rel='nofollow'>JS Array.length</a>
* <a href='http://forum.freecodecamp.com/t/javascript-array-prototype-push/14298' target='_blank' rel='nofollow'>JS Array.prototype.push()</a>
* <a href='http://forum.freecodecamp.com/t/javascript-array-prototype-sort/14306' target='_blank' rel='nofollow'>JS Array.prototype.sort()</a>

</details>
<details><summary>Solution 2 (Click to Show/Hide)</summary>


```javascript
function updateInventory(arr1, arr2) {
  // All inventory must be accounted for or you're fired!

  var index;
  var arrCurInvName = []; // Names of arr1's items
  var arrNeInvName = []; // Names of arr2's items

  // Same as using two for loops, this takes care of increasing the number of stock quantity.

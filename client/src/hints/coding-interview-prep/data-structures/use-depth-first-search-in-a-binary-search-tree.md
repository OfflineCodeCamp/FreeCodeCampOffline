﻿# [Use Depth First Search in a Binary Search Tree](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/use-depth-first-search-in-a-binary-search-tree)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
var displayTree = tree => console.log(JSON.stringify(tree, null, 2));
function Node(value) {
  this.value = value;
  this.left = null;
  this.right = null;
}
function BinarySearchTree() {
  this.root = null;
  // Only change code below this line
  // In-order traversal
  this.inorder = function() {
    if (this.root === null) return null;
    const nodes = [];

﻿# [Insert an Element into a Max Heap](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/insert-an-element-into-a-max-heap)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
var MaxHeap = function() {
  this.heap = [null];

  this.insert = (ele) => {
    var index = this.heap.length;
    var arr = [...this.heap];
    arr.push(ele);

    while (ele &gt; arr[Math.floor(index / 2)] &amp;& index > 1) {
      arr[index] = arr[Math.floor(index / 2)];
      arr[Math.floor(index / 2)] = ele;
      index = arr[Math.floor(index / 2)];
    }

    this.heap = arr;
  }

  this.print = () => {
    return this.heap.slice(1);
  }
};
```

</details>
﻿# [Find the Minimum and Maximum Value in a Binary Search Tree](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/find-the-minimum-and-maximum-value-in-a-binary-search-tree)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
var displayTree = tree => console.log(JSON.stringify(tree, null, 2));
function Node(value) {
  this.value = value;
  this.left = null;
  this.right = null;
}
function BinarySearchTree() {
  this.root = null;
  // Only change code below this line
  this.findMin = function() {
    if (!this.root) return null;
    // Find leftmost node
    let curr = this.root;
    while (curr.left) {
      curr = curr.left;
    }
    return curr.value;
  }

  this.findMax = function() {
    if (!this.root) return null;
    // Find rightmost node
    let curr = this.root;
    while (curr.right) {
      curr = curr.right;
    }
    return curr.value;
  }
  // Only change code above this line
}
```
[/details]
﻿# [Create a Priority Queue Class](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/create-a-priority-queue-class)

---
## Problem Explanation
- Priority Queue is an Abstract Data Type.
- It can be implemented using other Data Structures but is commonly implemented using a Heap.
- Each node contains a priority. When we enqueue a node to the queue, it's "bubbled up" to its place in the queue.
- In this challenge we need to create a Priority Queue class based on Min-Heap with a few methods:
  - enqueue() - It enqueue's the new node to it's appropriate place in queue.
  - dequeue() - It removes the first node from the queue.
  - front() - This method returns the first node to be dequeued.
  - size() - Returns the size of the queue.
  - isEmpty() - Returns if the queue is empty.
- | DS    | Access | Search | Insert | Delete |
  | ----- | ------ | ------ | ------ | ------ |
  | Priority Queue |   1    |    1   |   logn    |    logn   |

### Relevant Links
- [Wikipedia](https://en.wikipedia.org/wiki/Priority_queue)


---
## Solutions
<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js

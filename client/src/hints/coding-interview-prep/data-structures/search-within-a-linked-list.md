﻿# [Search within a Linked List](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/search-within-a-linked-list)

---
## Solutions


<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function LinkedList() {
  var length = 0;
  var head = null;

  var Node = function(element){ // {1}
    this.element = element;
    this.next = null;
  };

  this.size = function() {
    return length;
  };

  this.head = function(){
    return head;
  };

  this.add = function(element){
    var node = new Node(element);
    if(head === null){

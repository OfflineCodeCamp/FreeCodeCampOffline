﻿# [Work with Nodes in a Linked List](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/work-with-nodes-in-a-linked-list)

---

Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
var Node = function(element) {
  this.element = element;
  this.next = null;
};
var Kitten = new Node(&#39;Kitten');
var Puppy = new Node(&#39;Puppy');

Kitten.next = Puppy;
// Only change code below this line
const Cat = new Node('Cat');
const Dog = new Node('Dog');

Kitten.next = Puppy;
Puppy.next = Cat;
Cat.next = Dog;
```
[/details]
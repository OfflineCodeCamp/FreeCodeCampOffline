﻿# [Adjacency Matrix](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/adjacency-matrix)
---
Solutions
---
[details=&quot;Solution 1 (Click to Show/Hide)&quot;]

```
const adjMatUndirected = [
  // 1 2 3 4 5
  [0, 0, 1, 1, 0], // 1
  [0, 0, 0, 0, 0], // 2
  [1, 0, 0, 0, 1], // 3
  [1, 0, 0, 0, 1], // 4
  [0, 0, 1, 1, 0]  // 5
];
```
[/details]
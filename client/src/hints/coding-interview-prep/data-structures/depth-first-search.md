﻿# [Depth-First Search](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/depth-first-search)
This is a stub. Help the community by [making a suggestion](https://forum.freecodecamp.org/new-topic?category=Contributors&title=&body=**What%20is%20your%20hint%20or%20solution%20suggestion%3F**%0A%0A%0A%0A%0A**Challenge%3A**%20Depth-First%20Search%0A%0A%0A**Link%20to%20the%20challenge%3A**%0A%0Ahttps%3A%2F%2Fwww.freecodecamp.org%2Flearn%2Fcoding-interview-prep%2Fdata-structures%2Fdepth-first-search) of a hint and/or solution.  We may use your suggestions to update the missing sections.

---

## Problem Explanation

This summarizes what need to be done without just restating the challenge description and/or instructions. This is an optional section

#### Relevant Links

- [Link Text](link_url_goes_here)
- [Link Text](link_url_goes_here)

---

## Hints

### Hint 1

Hint goes here

### Hint 2

Hint goes here

---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
function isFound(r, i) {
  for(let k = 0; k < r.length; ++k) {
    if(r[k] == i) {
      return true;
    }
  }
  return false;
}

function dfs(graph, root) {
  let r = [];
  let stack = [];
  stack.push(graph[root]);
  r.push(root);
  while(stack.length > 0) {
    for(let i = 0 ; i < graph[root].length; ++i) {
      if(!isFound(r, i) && stack[stack.length - 1][i] == 1) {
        stack.push(graph[i]);
        r.push(i);
        i=0;
      }
    }
    stack.pop()
  }
  return r;
}

var exDFSGraph = [
  [0, 1, 0, 0],
  [1, 0, 1, 0],
  [0, 1, 0, 1],
  [0, 0, 1, 0]
];
console.log(dfs(exDFSGraph, 3));

```

#### Code Explanation

- Code explanation goes here

#### Relevant Links

- [Link Text](link_url_goes_here)

</details>
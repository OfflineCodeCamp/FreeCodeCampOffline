﻿# [Check if an Element is Present in a Binary Search Tree](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/check-if-an-element-is-present-in-a-binary-search-tree)

---

Solutions
---


[details="Solution 1 (Click to Hide/Show)"]
```
var displayTree = tree => console.log(JSON.stringify(tree, null, 2));
function Node(value) {
  this.value = value;
  this.left = null;
  this.right = null;
}
function BinarySearchTree() {
  this.root = null;
  // Only change code below this line
  this.isPresent = function(data, node = this.root) {
    if (node === null) {
      return false;
    } else if (data &lt; node.value) {
      return this.isPresent(data, node.left);
    } else if (data &gt; node.value) {
      return this.isPresent(data, node.right);
    } else {
      return true;
    }
  }
  // Only change code above this line
}
```
[/details]
﻿# [Add a New Element to a Binary Search Tree](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/add-a-new-element-to-a-binary-search-tree)
This is a stub. Help the community by [making a suggestion](https://forum.freecodecamp.org/new-topic?category=Contributors&title=&body=**What%20is%20your%20hint%20or%20solution%20suggestion%3F**%0A%0A%0A%0A%0A**Challenge%3A**%20Add%20a%20New%20Element%20to%20a%20Binary%20Search%20Tree%0A%0A%0A**Link%20to%20the%20challenge%3A**%0A%0Ahttps%3A%2F%2Fwww.freecodecamp.org%2Flearn%2Fcoding-interview-prep%2Fdata-structures%2Fadd-a-new-element-to-a-binary-search-tree) of a hint and/or solution.  We may use your suggestions to update this stub.

---

## Problem Explanation

This summarizes what need to be done without just restating the challenge description and/or instructions. This is an optional section

#### Relevant Links

- [Link Text](link_url_goes_here)
- [Link Text](link_url_goes_here)

---

## Hints

### Hint 1

Hint goes here

### Hint 2

Hint goes here

---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
var displayTree = tree => console.log(JSON.stringify(tree, null, 2));
function Node(value) {
  this.value = value;
  this.left = null;
  this.right = null;
}
function BinarySearchTree() {

  this.root = null;
  // change code below this line
  this.add = function (integer) {

    let curr = this.root;

    if (!curr) {
      this.root = new Node(integer);
      return undefined;
    };
    
    while (curr) {
      if (integer < curr.value) {
        if (!curr.left) {
          curr.left = new Node(integer);
          return undefined;
        }
        curr = curr.left;
      }
      else if (integer > curr.value) {
        if (!curr.right) {
          curr.right = new Node(integer);
          return undefined;
        }
        curr = curr.right;
      }
      else return null;
    }
  }   
}
```

#### Code Explanation

- Code explanation goes here

#### Relevant Links

- [Link Text](link_url_goes_here)

</details>
﻿# [Evaluate binomial coefficients](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/evaluate-binomial-coefficients)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function binom(n, k) {
    return factorial(n)/(factorial(n-k)*factorial(k))
}

function factorial(n) {
  return n &lt;= 1 ? 1 : n * factorial(n - 1);
}
```

This is based on the solution for the Factorial challenge, as the final answer is composed from three factorials. The factorials are calculated through a recursive, conditional loop until 1 is reached.

</details>
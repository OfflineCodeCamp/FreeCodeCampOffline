﻿# [Ackermann function](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/ackermann-function)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function ack(m, n) {
   return (m === 0)
     ? n + 1
     : (m &gt; 0 &amp;& n === 0)
       ? ack(m-1, 1)
       : ack(m-1, ack(m, n-1))
}
```

This solution writes the function in ternary format. Because the function is provided, all we need to do is to write the function to match exactly the equation provided.

</details>
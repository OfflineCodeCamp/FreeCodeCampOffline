﻿# [Accumulator factory](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/accumulator-factory)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function accumulator(sum) {
    return (n) => sum += n;
}

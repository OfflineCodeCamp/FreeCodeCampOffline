﻿# [FizzBuzz](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/fizzbuzz)

---
## Problem Explanation

No article for now, you can enjoy a video explanation:
https://www.youtube.com/watch?v=QPZ0pIK_wsc

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function fizzBuzz() {
    let res=[];
    for (let i =1; i < 101; i++) {
        if (i % 3 === 0  && i % 5 === 0) {
            res.push("FizzBuzz");
        }
        else if (i % 3 === 0) {
            res.push("Fizz");
        }
        else if (i % 5 === 0) {
            res.push("Buzz");
        } 
        else {
            res.push(i);
        }
    }
    return res;
}
```

</details>
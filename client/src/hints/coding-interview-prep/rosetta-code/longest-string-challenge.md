﻿# [Longest string challenge](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/longest-string-challenge)

---
Solutions
---

[details="Solution 1 (Click to Show/Hide)&quot;]
```
function longestString(strings) {
  const maxLength = strings.reduce(
    (maxLength, string) =&gt; Math.max(maxLength, string.length),
    0);
  return strings.filter((string) => string.length == maxLength)
}
```
[/details]
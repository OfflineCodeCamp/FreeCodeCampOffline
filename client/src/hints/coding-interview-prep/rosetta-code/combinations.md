﻿# [Combinations](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/combinations)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function combinations(k, arr, prefix=[]) {
    if (prefix.length == 0) arr = [...Array(arr).keys()];
    if (k === 0) return [prefix];
    return arr.flatMap((v, i) =&gt;
        combinations(k-1, arr.slice(i+1), [...prefix, v])
    );
}
```
[/details]
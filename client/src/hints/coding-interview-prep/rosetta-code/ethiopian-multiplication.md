﻿# [Ethiopian multiplication](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/ethiopian-multiplication)

---
## Problem Explanation

Ethiopian multiplication is a method of multiplying integers using only addition, doubling, and halving. Implement the Ethiopian multiplication algorithm.

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function eth_mult(a, b) {
  // required functions
  const halve = (n) => Math.floor(n/2);
  const double = (n) => n + n;
  const isEven = (n) => n % 2 === 0;

  // setup variables
  let [left, right] = [a, b].sort((a, b) => a - b);
  let total = isEven(left) ? 0 : right;


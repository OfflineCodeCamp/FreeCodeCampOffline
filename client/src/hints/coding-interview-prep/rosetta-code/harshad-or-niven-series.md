﻿# [Harshad or Niven series](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/harshad-or-niven-series)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```javascript
function isHarshadOrNiven(n) {
  const harshads = [];
  let current = n;
  while (harshads.length < 10) {
    current++;
    const divisor = current
      .toString()
      .split("")
      .map(digit =&gt; parseInt(digit))
      .reduce((sum, digit) =&gt; sum + digit);
    if (current % divisor == 0) {
      harshads.push(current);
    }
  }
  return harshads;
}
```
[/details]
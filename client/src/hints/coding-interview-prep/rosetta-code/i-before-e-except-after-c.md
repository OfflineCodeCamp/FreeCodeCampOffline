﻿# [I before E except after C](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/i-before-e-except-after-c)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function IBeforeExceptC(word) {
  return word.indexOf("cei") !== -1;
}
```
[/details]
﻿# [Date format](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/date-format)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```javascript
function getDateFormats() {
  const weekdays = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  const now = new Date();
  return [
    now.getFullYear() + "-" + (now.getMonth()+1) + "-" + now.getDate(),
    weekdays[now.getDay()] + ", " + months[now.getMonth()] + ' ' + now.getDate() + ", " + now.getFullYear()
  ];
}
```
[/details]
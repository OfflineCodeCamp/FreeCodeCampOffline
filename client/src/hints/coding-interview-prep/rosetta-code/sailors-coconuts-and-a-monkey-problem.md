﻿# [Sailors, coconuts and a monkey problem](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/sailors-coconuts-and-a-monkey-problem)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```javascript
function splitCoconuts(n) {
  // https://oeis.org/A002021
  return n % 2 === 0 ?
    (n - 1) * (n**n - 1) :
    n**n - n + 1;
}
```
[/details]
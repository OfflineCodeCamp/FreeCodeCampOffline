﻿# [Generator/Exponential](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/generator-exponential)
---
Solutions
---
[details="Solution 1 (Click to Show/Hide)"]

```
function exponentialGenerator(n) {
  const firstNumbers = [];
  // Only change code below this line
  for (let i = 2; firstNumbers.length &lt; n; i++) {
    if (!Number.isInteger(Math.cbrt(i**2))) {
      firstNumbers.push(i**2)
    }
  }
  return firstNumbers[n-1];
}
```
[/details]
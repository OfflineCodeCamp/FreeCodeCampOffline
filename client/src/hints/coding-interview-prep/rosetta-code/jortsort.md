﻿# [JortSort](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/jortsort)
---
Solutions
---
[details=&quot;Solution 1 (Click to Show/Hide)&quot;]

```javascript
function jortsort(array) {
  const array1 = JSON.stringify(array);
  return JSON.stringify(array.sort((a, b) => a - b)) === array1;
}
```
[/details]
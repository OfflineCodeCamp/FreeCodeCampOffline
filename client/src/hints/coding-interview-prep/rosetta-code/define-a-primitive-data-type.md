﻿# [Define a primitive data type](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/define-a-primitive-data-type)
---
Solutions
---
[details="Solution 1 (Click to Show/Hide)&quot;]

```javascript
function Num(n) {
  if (typeof(n) !== &#39;number') {
    throw TypeError("Not a Number");
  }
  if (n < 1 || n > 10) {
    throw TypeError("Out of range");
  }
  return new Number(n);
}
```
[/details]
﻿# [Iterated digits squaring](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/iterated-digits-squaring)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```javascript
function iteratedSquare(n) {
  const squaredN = n
    .toString()
    .split('')
    .reduce((sum, digit) => sum + parseInt(digit) ** 2, 0);

  return squaredN === 1 || squaredN === 89 ?
    squaredN :
    iteratedSquare(squaredN)
}
```
[/details]

[details=&quot;Solution 2 (Click to Show/Hide)"]
```javascript
function iteratedSquare(n) {
  let squaredN = n;
  while (squaredN !== 1 && squaredN !== 89) {
    squaredN = squaredN
      .toString()
      .split('')
      .reduce((sum, digit) => sum + parseInt(digit) ** 2, 0);
  }
  return squaredN;
}
```
[/details]
﻿# [Look-and-say sequence](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/look-and-say-sequence)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```javascript
function lookAndSay(str) {
  const strArr = str.split('');
  let countStr = &#39;';
  let charCurr = strArr[0];
  let charCount = 1;
  for (let i = 1; i &lt; strArr.length; i++) {
    if (strArr[i] === charCurr) {
      charCount++;
    } else {
      countStr += charCount + charCurr;
      charCurr = strArr[i];
      charCount = 1;
    }
  }
  return countStr + charCount + charCurr;
}
```
[/details]
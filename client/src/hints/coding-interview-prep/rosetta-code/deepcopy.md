﻿# [Deepcopy](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/deepcopy)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)&lt;/summary&gt;

```javascript
function deepcopy(obj) {
  return JSON.parse(JSON.stringify(obj));
}
```
First convert the object to a JSON string, then parse the string to build the object

</details>
﻿# [Count occurrences of a substring](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/count-occurrences-of-a-substring)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```javascript
function countSubstring(str, subStr) {
  let i = 0;
  let count = 0;
  const subStrLen = subStr.length;
  const upperBound = str.length - subStrLen;
  while (i < upperBound) {
    if (str.substr(i, subStrLen) === subStr) {
      count++;
      i += subStrLen;
    } else {
      i++;
    }
  }
  return count;
}
```

﻿# [Count the coins](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/count-the-coins)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function countCoins(cents) {
  const operands = [1, 5, 10, 25];
  const targetsLength = cents + 1;
  const operandsLength = operands.length;
  const t = [1];

  for (let a = 0; a &lt; operandsLength; a++) {
    for (let b = 1; b &lt; targetsLength; b++) {
      // initialise undefined target
      t[b] = t[b] ? t[b] : 0;

      // accumulate target + operand ways
      t[b] += (b < operands[a]) ? 0 : t[b - operands[a]];
    }
  }

  return t[targetsLength - 1];
}
```
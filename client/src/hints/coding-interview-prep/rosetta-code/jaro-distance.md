﻿# [Jaro distance](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/jaro-distance)

Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```javascript
// Inspired by Java Jaro distance code
//   https://rosettacode.org/wiki/Jaro_similarity#Java
function jaro(s1, s2) {

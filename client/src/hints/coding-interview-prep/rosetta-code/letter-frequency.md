﻿# [Letter frequency](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/letter-frequency)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function letterFrequency(txt) {
  var cs = txt.split(''),
    i = cs.length,
    dct =  {},
    c = &#39;&#39;,
    keys;

  while (i--) {
    c = cs[i];
    dct[c] = (dct[c] || 0) + 1;
  }

  keys = Object.keys(dct);
  keys.sort();
  return keys.map(function (c) { return [c, dct[c]]; });
}
```
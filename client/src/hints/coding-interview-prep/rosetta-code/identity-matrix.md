﻿# [ Rosetta Code: Identity matrix](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/identity-matrix)

---
## Solutions

[details="Solution 1"]
```javascript
function idMatrix(n) {
    // array of values [0, 1, ... n]
    const indexArr = [...Array(n).fill(0).keys()];
    // array of arrays with 1 if row and column match, else 0
    return indexArr.map(i =&gt; indexArr.map(j =&gt; i === j ? 1 : 0));
}
```
[/details]
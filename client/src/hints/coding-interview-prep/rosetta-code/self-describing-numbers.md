﻿# [Self Describing Numbers](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/self-describing-numbers)

---

Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function isSelfDescribing(n) {
  let digits = n.toString().split(""); // Array of digits
  // Check that every digit is as frequent as the value in that position
  return digits.every(
    (targetCount, position) =&gt;
      // Check number of matching digits for current position
      targetCount == digits.filter(num =&gt; num == position).length
    );
}
```
[/details]
﻿# [Hofstadter Q sequence](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/hofstadter-q-sequence)

Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```javascript
// Memoized values
const Q = Array(2500).fill(0);
Q[1] = 1; Q[2] = 1;

function hofstadterQ(n) {
  if (!n || n < 1)
    // No argument or invalid argument
    return &quot;integer&quot;;
  else if (Q[n])
    // Value already computed and stored
    return Q[n];
  else
    // Value needs to be computed and stored
    return Q[n] = hofstadterQ(n-hofstadterQ(n-1)) + hofstadterQ(n-hofstadterQ(n-2));
}
```
[/details]
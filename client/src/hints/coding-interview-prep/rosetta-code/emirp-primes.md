﻿# [Emirp primes](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/emirp-primes)

Solutions
---


[details="Solution 1 (Click to Show/Hide)"]
```javascript
function isPrime(n) {
  const limit = Math.sqrt(n);
  if (n % 2 === 0)
    return false;
  for (let i = 3; i <= limit; i += 2) {
    if (n % i === 0)
      return false;
  }
  return true;
};

function isEmirp(n) {
  const r = n
    .toString()
    .split("")
    .reverse()
    .join("");

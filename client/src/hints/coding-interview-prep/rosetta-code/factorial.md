﻿# [Factorial](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/factorial)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary&gt;

```javascript
function factorial(n) {
    return n &lt;= 1 ? 1 : n * factorial(n - 1);
}
```

The factorials are calculated through a recursive, conditional loop until 1 is reached.

This solution can also be represented as an arrow function:

```javascript
const factorial = n =>
    n <= 1 ? 1 : n * factorial(n - 1);
```

</details>
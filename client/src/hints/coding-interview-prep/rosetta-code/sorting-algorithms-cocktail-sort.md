﻿# [Sorting algorithms/Cocktail sort](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/sorting-algorithms-cocktail-sort)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function cocktailSort(arr) {
  let isSorted = true;
  while (isSorted) {
    for (let i = 0; i < arr.length - 1; i++) {
      if (arr[i] > arr[i + 1]) {
        let temp = arr[i];
        arr[i] = arr[i + 1];
        arr[i + 1] = temp;
        isSorted = true;
      }
    }

    if (!isSorted)
      break;

    isSorted = false;

    for (let j = arr.length - 1; j > 0; j--) {
      if (arr[j - 1] > arr[j]) {
        let temp = arr[j];
        arr[j] = arr[j - 1];
        arr[j - 1] = temp;
        isSorted = true;
      }
    }
  }

  return arr;
}
```

</details>
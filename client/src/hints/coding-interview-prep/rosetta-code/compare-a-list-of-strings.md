﻿# [Compare a list of strings](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/compare-a-list-of-strings)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```javascript
function allEqual(arr) {
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] != arr[i - 1])
      return false;
  }
  return true;
}

function azSorted(arr) {
  for (let i = 1; i &lt; arr.length; i++) {
    if (arr[i] &lt;= arr[i - 1])
      return false;
  }
  return true;
}
```
[/details]
﻿# [Loop over multiple arrays simultaneously](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/loop-over-multiple-arrays-simultaneously)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```javascript
function loopSimult(A) {
  const concatArr = Array(A[0].length).fill(&#39;');
  for (let i = 0; i &lt; A[0].length; i++) {
    for (let j = 0; j < A.length; j++) {
      concatArr[i] += A[j][i];
    }
  }
  return concatArr;
}
```
[/details]
﻿# [Linear congruential generator](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/linear-congruential-generator)
---
Solutions
---
[details=&quot;Solution 1 (Click to Show/Hide)&quot;]
```javascript
function linearCongGenerator(r0, a, c, m, n) {
  return n === 0 ?
    r0 :
    (a * linearCongGenerator(r0, a, c, m, n - 1) + c) % m;
}
```
[/details]
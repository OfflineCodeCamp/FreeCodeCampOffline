﻿# [Hash from two arrays](https://www.freecodecamp.org/learn/coding-interview-prep/rosetta-code/hash-from-two-arrays)

---

Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```javascript
function arrToObj(keys, vals) {
  let hash = {};
  for (let i = 0; i < keys.length; i++) {
    hash[keys[i]] = vals[i];
  }
  return hash;
}
```
[/details]


[details="Solution 2 (Click to Show/Hide)"]
```javascript
function arrToObj(keys, vals) {
  return keys.reduce(
    (hash, key, index) =&gt; {
      hash[key] = vals[index];
      return hash;
    }, {})
  }
```
[/details]
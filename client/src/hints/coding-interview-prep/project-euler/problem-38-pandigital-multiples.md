﻿# [Problem 38: Pandigital multiples](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-38-pandigital-multiples)

---

## Problem Explanation

This summarizes what need to be done without just restating the challenge description and/or instructions. This is an optional section

#### Relevant Links

* Link Text
* Link Text

---

## Hints

### Hint 1

Hint goes here

### Hint 2

Hint goes here

---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
function pandigitalMultiples() {
  const nDigit = 9;

  function get9DigitConcatenatedProduct(num) {
    // returns false if concatenated product is not 9 digits
    let concatenatedProduct = num.toString();
    for (let i = 2; concatenatedProduct.length < nDigit; i++) {
      concatenatedProduct += num * i;
    }
    return concatenatedProduct.length === nDigit ? concatenatedProduct : false;
  }

  function is1to9Pandigital(num) {
    const numStr = num.toString();

    // check if length is not 9
    if (numStr.length !== nDigit) {
      return false;
    }

    // check if pandigital
    for (let i = nDigit; i > 0; i--) {
      if (numStr.indexOf(i.toString()) === -1) {
        return false;
      }
    }
    return true;
  }

  let largestNum = 0;
  for (let i = 9999; i >= 9000; i--) {
    const concatenatedProduct =  get9DigitConcatenatedProduct(i);
    if (is1to9Pandigital(concatenatedProduct) && concatenatedProduct > largestNum) {
      largestNum = parseInt(concatenatedProduct);
      break;
    }
  }
  return largestNum;
}
```

</details>

<details><summary>Solution 2 (Click to Show/Hide)</summary>

```
function pandigitalMultiples() {
  const nDigits = 9;
  let bigPandigital = 918273645; // Given in prompt
  // Loop through possible range of values
  //   i must have fewer than 5 digits
  for (let i = 1; i < 10000; i++) {
    // Build pandigital candidate
    let candidate = i.toString() + (2*i).toString();
    let j = 3;
    while (candidate.length < nDigits) {
      candidate += i*j;
      j++;
    }
    // Check candidate
    if (candidate.length === nDigits && candidate > bigPandigital) {
      // Test for pandigital
      let isPandigital = true;
      for (let j = 1; isPandigital && j <= nDigits; j++) {
        isPandigital = candidate.indexOf(j.toString()) >= 0;
      }
      // Update largest
      if (isPandigital) {
        bigPandigital = Number(candidate);
      }
    }
  }
  return bigPandigital;
}

console.log(pandigitalMultiples());
```

#### Code Explanation

- In this solution, we build and check all viable candidates for pandigital numbers that are concatenated products.

- The final number must have exactly 9 digits, and must be formed as `candidate = i * 1 + i * 2 + ...`, so we need to check integers with no more than `4` digits.

- A pandigital number has exactly 9 digits, with each unique digit represented only once

</details>



<details><summary>Solution 3 (Click to Show/Hide)</summary>

```
function isPandigital(str) {
  return new Set([...str.replace(/0/g, '')]).size === str.length && str.length === 9;
}

function concatiply (max, num) {
  return [...Array(max)].reduce((a, _, i) => a + num * (i + 1), '');
}

function pandigitalMultiples() {
  for (let i = 9876; i > 0; i--) {
    const product = concatiply(2, i);
    if (isPandigital(product)) return +product;
  }
}
```

#### Code Explanation

* In this solution, we build and check all viable candidates for pandigital numbers that are concatenated products from 4 digit numbers.
* We know the candidate must be of the form `9xxxxxxxx` because in the prompt we are given the pandigital number from a concatenated product `918273645`.
* We know the pandigitial number cannot come from a 2 digit number as the concatenated prime of that form will be `9x xxx xxx` or `9x xxx xxx xxx`, which does not have the correct number of digits.
* We know the pandigitial number cannot come from a 3 digit number as the concatenated prime of that form will be `9xx xxxx` or `9xx xxxx xxxx`, which does not have the correct number of digits.
* Thus, then number must come from a 4 digit number and be of the form `9xxx xxxx`.
* We start with `i = 9876` and decrement until we find a pandigital number formed as a concatenated product. If no such number is found, then we return `918273645` from the prompt.
[/details]
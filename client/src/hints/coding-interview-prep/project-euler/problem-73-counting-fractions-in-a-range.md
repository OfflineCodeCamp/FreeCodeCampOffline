﻿# [Problem 73: Counting fractions in a range](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-73-counting-fractions-in-a-range)

---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function countingFractionsInARange(limit) {
  let result = 0;
  const stack = [[3, 2]];
  while (stack.length > 0) {
    const [startDenominator, endDenominator] = stack.pop();
    const curDenominator = startDenominator + endDenominator;
    if (curDenominator &lt;= limit) {
      result++;
      stack.push([startDenominator, curDenominator]);
      stack.push([curDenominator, endDenominator]);
    }
  }
  return result;
}
```
[/details]
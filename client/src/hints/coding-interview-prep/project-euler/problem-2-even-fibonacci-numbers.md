﻿# [Problem 2: Even Fibonacci Numbers](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-2-even-fibonacci-numbers)

---
## Problem Explanation
- A fibonacci sequence is a sequence where `fib(n) = fib(n-1) + fib(n-2)`.

﻿# [Problem 63: Powerful digit counts](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-63-powerful-digit-counts)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function powerfulDigitCounts(n) {
  function countDigits(num) {
    let counter = 0;
    while (num > 0) {
      num = Math.floor(num / 10);
      counter++;
    }
    return counter;
  }

  let numbersCount = 0;

  let curNum = 1;
  while (curNum &lt; 10) {
    let power = n;
    if (power === countDigits(curNum ** power)) {
      numbersCount++;
    }
    curNum++;
  }

  return numbersCount;
}
```
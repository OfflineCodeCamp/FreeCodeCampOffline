﻿# [Problem 3: Largest prime factor](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-3-largest-prime-factor)

---
## Problem Explanation
- To find the largest prime factor of a number, we start from the smallest prime factor 2 and divide the number with it.
- If the remainder is 0 that means the number is divisible by that prime number, we keep dividing the number by same prime number until that number is no more divisible by that prime number. 
- After that, we incrememnt the prime factor by 1 and repeat this process till the number becomes 1.



---

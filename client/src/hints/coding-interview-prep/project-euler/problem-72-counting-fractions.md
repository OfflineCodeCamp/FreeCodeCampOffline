﻿# [Problem 72: Counting fractions](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-72-counting-fractions)

---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function countingFractions(limit) {
  const phi = {};
  let count = 0;

  for (let i = 2; i &lt;= limit; i++) {
    if (!phi[i]) {
      phi[i] = i;
    }
    if (phi[i] === i) {
      for (let j = i; j &lt;= limit; j += i) {
        if (!phi[j]) {
          phi[j] = j;
        }
        phi[j] = (phi[j] / i) * (i - 1);
      }
    }
    count += phi[i];
  }

  return count;
}
```
[/details]
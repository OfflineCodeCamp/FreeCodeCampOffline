﻿# [Problem 78: Coin partitions](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-78-coin-partitions)

---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function coinPartitions(divisor) {
  const partitions = [1];

  let n = 0;
  while (partitions[n] !== 0) {
    n++;
    partitions.push(0);

    let i = 0;
    let pentagonal = 1;
    while (pentagonal &lt;= n) {
      const sign = i % 4 &gt; 1 ? -1 : 1;
      partitions[n] += sign * partitions[n - pentagonal];
      partitions[n] = partitions[n] % divisor;

      i++;

      let k = Math.floor(i / 2) + 1;
      if (i % 2 !== 0) {
        k *= -1;
      }
      pentagonal = Math.floor((k * (3 * k - 1)) / 2);
    }
  }
  return n;
}
```
[/details]
﻿# [Problem 1: Multiples of 3 and 5](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-1-multiples-of-3-and-5)

---
## Problem Explanation
- We can find if a number is divisble by another number with the help of `%` modulo operator.
- `num1 % num2` returns `0` if there's no remainder while doing `num1/num2`.
- Starting from `i = 3` because that's the first number that's divisble by 3 or 5, we loop through till the `number` provided.
- If the number is divisible either by 3 or 5, we add that to the variable `sum` and finally return it.


---
## Solutions


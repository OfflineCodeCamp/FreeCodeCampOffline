﻿# [Problem 22: Names scores](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-22-names-scores)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
function nameScoreCalc(word) {
  let sum = 0;
  let alphabets = ["", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
  for (let i = 0 ; i < word.length; i++)
    sum += alphabets.indexOf(word[i].toLowerCase());
  return sum;
}
function namesScores(arr) {
  arr = [...arr].sort();
  arr.unshift('');
  let total = 0;
  for (let i = 1 ; i < arr.length; i++)
    total += nameScoreCalc(arr[i]) * i;
  return total;
}
const test1 = ['THIS', 'IS', 'ONLY', 'A', 'TEST'];
const test2 = ['I', 'REPEAT', 'THIS', 'IS', 'ONLY', 'A', 'TEST'];
```
</details>

<details><summary>Solution 2 (Click to Show/Hide)</summary>

```js
function nameScoreCalc(word) {
  return [ ...word ]
  .reduce((sum, letter) => sum += letter.charCodeAt() - 64, 0);
}

function namesScores(arr) {
  return arr
    .sort((a, b) => a < b ? - 1: a > b ? 1 : 0)
    .reduce((total, word, i) => total += nameScoreCalc(word) * (i + 1), 0);
}
const test1 = ['THIS', 'IS', 'ONLY', 'A', 'TEST'];
const test2 = ['I', 'REPEAT', 'THIS', 'IS', 'ONLY', 'A', 'TEST'];
```
</details>
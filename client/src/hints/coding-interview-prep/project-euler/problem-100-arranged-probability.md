﻿# [Problem 100: Arranged probability](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-100-arranged-probability)

---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function arrangedProbability(limit) {
  // Based on https://www.mathblog.dk/project-euler-100-blue-discs-two-blue/
  let blue = 15;
  let discs = 21;

  while (discs &lt; limit) {
    const nextBlue = 3 * blue + 2 * discs - 2;
    const nextDiscs = 4 * blue + 3 * discs - 3;

    blue = nextBlue;
    discs = nextDiscs;
  }
  return blue;
}
```
[/details]
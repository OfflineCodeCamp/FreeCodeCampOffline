﻿# [Problem 99: Largest exponential](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-99-largest-exponential)

---
Solutions
---

[details="Solution 1 (Click to Show/Hide)&quot;]
```
function largestExponential(baseExp) {
  let maximum = 0;
  let indexOfMaximum = 0;
  for (let i = 1; i &lt; baseExp.length; i++) {
    const curValue = Math.log(baseExp[i][0]) * baseExp[i][1];
    if (curValue > maximum) {
      maximum = curValue;
      indexOfMaximum = i;
    }
  }

  return baseExp[indexOfMaximum];
}
```
[/details]
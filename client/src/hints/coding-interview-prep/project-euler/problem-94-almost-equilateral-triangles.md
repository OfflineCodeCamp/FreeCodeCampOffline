﻿# [Problem 94: Almost equilateral triangles](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-94-almost-equilateral-triangles)

---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function almostEquilateralTriangles(limit) {
  // Based on https://blog.dreamshire.com/project-euler-94-solution/
  let perimetersSum = 0;

  let sidesAB = 1;
  let sideC = 1;
  let perimeter = 0;
  let perimeterOffset = 1;

  while (perimeter &lt;= limit) {
    [sidesAB, sideC] = [4 * sidesAB - sideC + 2 * perimeterOffset, sidesAB];
    perimeterOffset = -perimeterOffset;

    perimetersSum += perimeter;
    perimeter = 3 * sidesAB - perimeterOffset;
  }

  return perimetersSum;
}
```
[/details]
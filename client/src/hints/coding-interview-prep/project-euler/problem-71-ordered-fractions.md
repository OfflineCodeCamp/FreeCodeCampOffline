﻿# [Problem 71: Ordered fractions](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-71-ordered-fractions)

---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function orderedFractions(limit) {
  const fractions = [];
  const fractionValues = {};
  const highBoundary = 3 / 7;
  let lowBoundary = 2 / 7;

  for (let denominator = limit; denominator > 2; denominator--) {
    let numerator = Math.floor((3 * denominator - 1) / 7);
    let value = numerator / denominator;
    if (value &gt; highBoundary || value &lt; lowBoundary) {
      continue;
    }
    fractionValues[value] = [numerator, denominator];
    fractions.push(value);
    lowBoundary = value;
  }

  fractions.sort();
  return fractionValues[fractions[fractions.length - 1]][0];
}
```
[/details]
﻿# [Problem 10: Summation of primes](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-10-summation-of-primes)

---
## Problem Explanation
- In this challenge we need to find sum of all prime numbers up to `n`.
- Example:
  - If `n = 10` then prime numbers before it are `2, 3, 5, 7` and their sum is `17`.


---
## Solutions

[details="Solution 1 - Divisibility checking (Click to Show/Hide)"]
```

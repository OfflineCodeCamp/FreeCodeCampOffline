﻿# [Problem 76: Counting summations](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-76-counting-summations)

---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function countingSummations(n) {
  const combinations = new Array(n + 1).fill(0);
  combinations[0] = 1;

  for (let i = 1; i &lt; n; i++) {
    for (let j = i; j &lt; n + 1; j++) {
      combinations[j] += combinations[j - i];
    }
  }
  return combinations[n];
}
```
[/details]
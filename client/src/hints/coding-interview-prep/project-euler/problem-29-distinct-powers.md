﻿# [Problem 29: Distinct powers](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-29-distinct-powers)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function distinctPowers(n) {
  let powers = {};
  for (let i = 2; i &lt;= n; i++) {
    for (let j = 2; j &lt;= n; j++) {
      powers[Math.pow(i, j)] = true; // Repeated instances will not add additional key
    }
  }
  return Object.keys(powers).length;
}
```
[/details]
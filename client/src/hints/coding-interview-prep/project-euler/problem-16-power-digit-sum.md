﻿# [Problem 16: Power digit sum](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-16-power-digit-sum)
---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function powerDigitSum(exponent) {
  // Take exponent and convert to string of digits
  const num = BigInt(Math.pow(2, exponent));
  const digits = num.toString().split(&#39;&#39;);
  // Sum digits in string representation
  return digits.reduce((sum, digit) => sum + parseInt(digit), 0);
}

powerDigitSum(15);
```
[/details]
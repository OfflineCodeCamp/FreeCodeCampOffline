﻿# [Problem 89: Roman numerals](https://www.freecodecamp.org/learn/coding-interview-prep/project-euler/problem-89-roman-numerals)

---
Solutions
---

[details="Solution 1 (Click to Show/Hide)"]
```
function romanNumerals(roman) {
  const numerals = [...roman];
  const replaces = [
    ['VIIII', 'IX'],
    ['IIII', 'IV'],
    ['LXXXX', 'XC'],
    ['XXXX', 'XL'],
    ['DCCCC', 'CM'],
    ['CCCC', 'CD']
  ];
  let savedChars = 0;
  for (let i = 0; i < numerals.length; i++) {
    const charsBefore = numerals[i].length;
    for (let j = 0; j < replaces.length; j++) {
      numerals[i] = numerals[i].replace(...replaces[j]);
    }
    const charsAfter = numerals[i].length;
    savedChars += charsBefore - charsAfter;
  }
  return savedChars;
}
```
[/details]
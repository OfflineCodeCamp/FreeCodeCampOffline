# [Use RGB values to Color Elements](https://www.freecodecamp.org/learn/responsive-web-design/basic-css/use-rgb-values-to-color-elements)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<style>
  body {
    background-color: rgb(0, 0, 0);
  }
</style>
```
</details>
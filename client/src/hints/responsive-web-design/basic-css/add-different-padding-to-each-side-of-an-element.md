# [Add Different Padding to Each Side of an Element](https://www.freecodecamp.org/learn/responsive-web-design/basic-css/add-different-padding-to-each-side-of-an-element)

---
## Problem Explanation

To adjust the padding of an element use:

```css
.example {
  padding: 10px;
}
```

To specify padding sizes on an element by individual sides we can use �padding-top�, �padding-right�, �padding-bottom�, and �padding-left�. We can use any of these in combination and in any order. For example:

```css
.example {
  padding-top: 5px;
  padding-bottom: 0px;
}
```

Or:

```css
.example {
  padding-top: 20px;
  padding-left: 25px;
  padding-right: 5px;
}
```
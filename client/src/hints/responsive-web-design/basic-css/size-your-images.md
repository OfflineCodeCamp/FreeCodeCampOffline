# [Size Your Images](https://www.freecodecamp.org/learn/responsive-web-design/basic-css/size-your-images)

---
## Problem Explanation
For sizing your images, first create your class in your style tag.

An example:
```css
<style>
  .fixed-image {
    width: 500px;
    height: auto;
  }
```
  
You can then add the class to your image:
```css
  <img class="fixed-image" src="http://www.example.com/picture"/>
```
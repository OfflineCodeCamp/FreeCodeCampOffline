# [Style the HTML Body Element](https://www.freecodecamp.org/learn/responsive-web-design/basic-css/style-the-html-body-element)

---
## Solutions


<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<style>
body {
  background-color: black;
}
</style>
```

</details>
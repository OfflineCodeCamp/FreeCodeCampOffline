# [Improve Compatibility with Browser Fallbacks](https://www.freecodecamp.org/learn/responsive-web-design/basic-css/improve-compatibility-with-browser-fallbacks)

---
## Problem Explanation
We need to add a fallback to the `background` property of the `.red-box` class.
This is to make sure that also older browsers that do not support variable syntax can still have a value to fall back on. (If a property has an invalid value, then the applied property will be the one above it)

**Example:**
If we need to add a fallback for  the background property of this `.black-box` class, we can do like this:
``` css
  :root {
    --black-color: black;
  }
  .black-box {
    background: black;  /* this is the browser fallback */
    background: var(--black-color);
    width: 100px;
    height: 100px;
  }
```


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

Add a fallback to the ```background``` property before the existing background declaration:

``` html
 <style>
  :root {
    --red-color: red;
  }
  .red-box {

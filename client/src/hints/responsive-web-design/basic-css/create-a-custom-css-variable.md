# [Create a custom CSS Variable](https://www.freecodecamp.org/learn/responsive-web-design/basic-css/create-a-custom-css-variable)

---
## Problem Explanation
We need to create a variable name ```--penguin-skin``` and give it a value of ```gray``` in the ```penguin``` class.

**Example:**

```css
--variable-name: value;
```


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

In the ```penguin``` class we create a variable name ```--penguin-skin``` and give it a value of ```gray```:

```css
--penguin-skin: gray;
```
</details>
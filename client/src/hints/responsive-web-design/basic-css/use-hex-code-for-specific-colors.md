# [Use Hex Code for Specific Colors](https://www.freecodecamp.org/learn/responsive-web-design/basic-css/use-hex-code-for-specific-colors)

---
## Problem Explanation
With CSS, we use 6 hexadecimal number to represent colors. For example, `#000000` is the lowest possible value, and it represents the color black.

This is the same as `#RRGGBB` which can also be simplified to `#RGB`.

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<style>
  body {
    background-color: #000000;
  }
</style>
```

</details>
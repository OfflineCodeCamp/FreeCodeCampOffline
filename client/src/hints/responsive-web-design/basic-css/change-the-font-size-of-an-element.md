# [Change the Font Size of an Element](https://www.freecodecamp.org/learn/responsive-web-design/basic-css/change-the-font-size-of-an-element)

---
## Problem Explanation
We need to create an entry for ```p``` elements and set the ```font-size``` to 16 pixels (```16px```), Inside the same ```<style>``` tag that contains your ```red-text``` class.

**Example:**

```css
h1 {
  font-size: 30px;
}
```


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```css
p {
  font-size: 16px;
}
```

</details>
# [Jump Straight to the Content Using the main Element](https://www.freecodecamp.org/learn/responsive-web-design/applied-accessibility/jump-straight-to-the-content-using-the-main-element)

---
Solutions
---


[details="Solution 1 (Click to Show/Hide)"]
```
<header>
  <h1>Weapons of the Ninja</h1>
</header&gt;
&lt;main>
</main>
<footer></footer>
```
[/details]
# [Avoid Colorblindness Issues by Carefully Choosing Colors that Convey Information](https://www.freecodecamp.org/learn/responsive-web-design/applied-accessibility/avoid-colorblindness-issues-by-carefully-choosing-colors-that-convey-information)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

Following the instructions:

Change the text color to a dark blue (#003366)

the line 4 becomes:

```css
color: #003366;
```

and the color of the text will become darker. 
</details>
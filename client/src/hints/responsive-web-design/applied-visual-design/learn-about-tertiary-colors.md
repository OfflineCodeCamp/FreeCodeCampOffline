# [Learn about Tertiary Colors](https://www.freecodecamp.org/learn/responsive-web-design/applied-visual-design/learn-about-tertiary-colors)

---
## Problem Explanation
Tertiary colours are combinations of primary and secondary colours. There are six tertiary colors; red-orange, yellow-orange, yellow-green, blue-green, blue-violet, and red-violet.

They are made by full saturation of a primary colour with half saturation of a secondary colour with no other colours.


---

# [Adjust the Size of a Header Versus a Paragraph Tag](https://www.freecodecamp.org/learn/responsive-web-design/applied-visual-design/adjust-the-size-of-a-header-versus-a-paragraph-tag)

---

## Problem Explanation

Currently, the `h4` element has the same `font-size` as all the other text on the page. This does not easily convey the importance of the header element.

To remedy this, increase the `font-size` of the `h4` element from its default of `16px` to `27px`/

#### Relevant Links

- [Mozilla Docs: h1-h6](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements)

---

## Hints

### Hint 1

You should add a font-size property to the h4 element inside the style tags and set it to 27 pixels.

---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<style>
  h4 {
    text-align: center;
    background-color: rgba(45, 45, 45, 0.1);
    padding: 10px;
    font-size: 27px;
  }
  p {
    text-align: justify;
  }
  .links {
    text-align: left;
    color: black;
  }
  .fullCard {
    width: 245px;
    border: 1px solid #ccc;
    border-radius: 5px;
    margin: 10px 5px;
    padding: 4px;
  }
  .cardContent {
    padding: 10px;
  }
  .cardText {
    margin-bottom: 30px;
  }
</style>
<div class="fullCard">
  <div class="cardContent">
    <div class="cardText">
      <h4>Alphabet</h4>
      <hr>
      <p><em>Google was founded by Larry Page and Sergey Brin while they were <u>Ph.D. students</u> at <strong>Stanford University</strong>.</em></p>
    </div>
    <div class="cardLinks">
      <a href="https://en.wikipedia.org/wiki/Larry_Page" target="_blank" class="links">Larry Page</a><br><br>
      <a href="https://en.wikipedia.org/wiki/Sergey_Brin" target="_blank" class="links">Sergey Brin</a>
    </div>
  </div>
</div>
```

#### Code Explanation

- The CSS `h4` element selector is already declared on the page. Adding the `font-size` property with a value of `27px` increases the _font-size_ of the `h4` element from its default of `16px`.

</details>
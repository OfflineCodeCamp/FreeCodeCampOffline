# [Push Elements Left or Right with the float Property](https://www.freecodecamp.org/learn/responsive-web-design/applied-visual-design/push-elements-left-or-right-with-the-float-property)

Add `float: left` to the `#left` selector and `float: right` to the `#right` selector.

---
## Solutions

<details><summary>Solution (Click to Show/Hide)</summary>

```css
#left {
  float: left;
  width: 50%;
}

#right {
  float: right;
  width: 40%;
}

aside, section {
  padding: 2px;
  background-color: #ccc;
}
```
</details>
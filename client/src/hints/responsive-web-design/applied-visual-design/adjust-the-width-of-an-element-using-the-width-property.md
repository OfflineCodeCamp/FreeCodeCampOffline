# [Adjust the Width of an Element Using the width Property](https://www.freecodecamp.org/learn/responsive-web-design/applied-visual-design/adjust-the-width-of-an-element-using-the-width-property)
**Hints**

**Hint #1**

Add the `width` property to the `full-card` class selector.

------------------------------------------------------

**Solutions**

[details="Solution 1 (click to show/hide)"]
```
<style>
  h4 {
    text-align: center;
  }
  p {
    text-align: justify;
  }
  .links {
    margin-right: 20px;
    text-align: left;
  }
  .fullCard {
    width: 245px;
    border: 1px solid #ccc;
    border-radius: 5px;
    margin: 10px 5px;
    padding: 4px;
  }
```

[/details]
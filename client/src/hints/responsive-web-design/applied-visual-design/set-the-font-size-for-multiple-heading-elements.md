# [Set the font-size for Multiple Heading Elements](https://www.freecodecamp.org/learn/responsive-web-design/applied-visual-design/set-the-font-size-for-multiple-heading-elements)
This is a stub. Help the community by [making a suggestion](https://forum.freecodecamp.org/new-topic?category=Contributors&title=&body=**What%20is%20your%20hint%20or%20solution%20suggestion%3F**%0A%0A%0A%0A%0A**Challenge%3A**%20Set%20the%20font-size%20for%20Multiple%20Heading%20Elements%0A%0A%0A**Link%20to%20the%20challenge%3A**%0A%0Ahttps%3A%2F%2Fwww.freecodecamp.org%2Flearn%2Fresponsive-web-design%2Fapplied-visual-design%2Fset-the-font-size-for-multiple-heading-elements) of a hint and/or solution.  We may use your suggestions to update the missing sections.

---

## Problem Explanation

This summarizes what need to be done without just restating the challenge description and/or instructions. This is an optional section

#### Relevant Links

- [Mozilla Docs: h tags](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements)

---

## Hints

### Hint 1

Hint goes here

### Hint 2

Hint goes here

---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<style>
  h1 {
    font-size: 68px;
  }
  h2 {
    font-size: 52px;
  }
  h3 {
    font-size: 40px;
  }
  h4 {
    font-size: 32px;
  }
  h5 {
    font-size: 21px;
  }
  h6 {
    font-size: 14px;
  }
</style>

<h1>This is h1 text</h1>

<h2>This is h2 text</h2>

<h3>This is h3 text</h3>

<h4>This is h4 text</h4>

<h5>This is h5 text</h5>

<h6>This is h6 text</h6>
```

#### Code Explanation

- Code explanation goes here

#### Relevant Links

- [Link Text](link_url_goes_here)
- [Link Text](link_url_goes_here)

</details>
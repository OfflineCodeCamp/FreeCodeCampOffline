# [Use the em Tag to Italicize Text](https://www.freecodecamp.org/learn/responsive-web-design/applied-visual-design/use-the-em-tag-to-italicize-text)

---

#### Relevant Links

- [Mozilla Docs on em](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/em)

---

## Hints

### Hint 1

Remember that the em tag should only wrap around contents of the p tag but not around the p tag itself.

---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<style>
  h4 {
    text-align: center;
    height: 25px;
  }
  p {
    text-align: justify;
  }
  .links {
    text-align: left;
    color: black;
  }
  .fullCard {
    width: 245px;
    border: 1px solid #ccc;
    border-radius: 5px;
    margin: 10px 5px;
    padding: 4px;
  }
  .cardContent {
    padding: 10px;
  }
  .cardText {
    margin-bottom: 30px;
  }
</style>
<div class="fullCard">
  <div class="cardContent">
    <div class="cardText">
      <h4>Google</h4>
      <p><em>Google was founded by Larry Page and Sergey Brin while they were <u>Ph.D. students</u> at <strong>Stanford University</strong>.</em></p>
    </div>
    <div class="cardLinks">
      <a href="https://en.wikipedia.org/wiki/Larry_Page" target="_blank" class="links">Larry Page</a><br><br>
      <a href="https://en.wikipedia.org/wiki/Sergey_Brin" target="_blank" class="links">Sergey Brin</a>
    </div>
  </div>
</div>

```

#### Code Explanation

- All of the contents within the paragraph element (`p`), including the period (`.`), are wrapped in the `em` tag, causing the text to be displayed in italics.

#### Relevant Links

- [Mozilla Docs on em](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/em)

</details>
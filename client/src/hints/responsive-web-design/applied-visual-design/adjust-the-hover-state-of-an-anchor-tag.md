# [Adjust the Hover State of an Anchor Tag](https://www.freecodecamp.org/learn/responsive-web-design/applied-visual-design/adjust-the-hover-state-of-an-anchor-tag)

---
## Problem Explanation
You can use the :hover pseudo-class with the anchor tag to style how a link appears when someone hovers over the link with their mouse or other pointing device. 

For example, the following code will change the color of the link to orange when someone hovers over it:

```html
<a href="https://www.freecodecamp.org/">freeCodeCamp</a>
```

```css
a:hover {
    color: orange;
}
```
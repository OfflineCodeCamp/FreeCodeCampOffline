# [Create a More Complex Shape Using CSS and HTML](https://www.freecodecamp.org/learn/responsive-web-design/applied-visual-design/create-a-more-complex-shape-using-css-and-html)

## Problem Explanation

This guide post is incomplete. Help us finish it, by [making a suggestion](https://forum.freecodecamp.org/new-topic?category=Contributors&title=&body=**What%20is%20your%20hint%20or%20solution%20suggestion%3F**%0A%0A%0A%0A%0A**Challenge%3A**%20Create%20a%20More%20Complex%20Shape%20Using%20CSS%20and%20HTML%0A%0A%0A**Link%20to%20the%20challenge%3A**%0A%0Ahttps%3A%2F%2Fwww.freecodecamp.org%2Flearn%2Fresponsive-web-design%2Fapplied-visual-design%2Fcreate-a-more-complex-shape-using-css-and-html).

#### Relevant Links

- [Mozilla Docs - ::before](https://developer.mozilla.org/en-US/docs/Web/CSS/::before)
- [Mozilla Docs - ::after](https://developer.mozilla.org/en-US/docs/Web/CSS/::after)

---

## Hints

### Hint 1

Hint goes here

---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<style>
  .heart {
    position: absolute;
    margin: auto;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: pink;
    height: 50px;
    width: 50px;
    transform: rotate(-45deg);
  }
  
  .heart::after {
    background-color: pink;
    content: "";
    border-radius: 50%;
    position: absolute;
    width: 50px;
    height: 50px; 
    top: 0px;
    left: 25px;
  }

  .heart::before {
    content: "";
    background-color: pink;
    border-radius: 50%;
    position: absolute;
    width: 50px;
    height: 50px;
    top: -25px;
    left: 0px;
  }
</style>
<div class="heart"></div>
```

#### Code Explanation

- Code explanation goes here
- Code explanation goes here

#### Relevant Links

</details>
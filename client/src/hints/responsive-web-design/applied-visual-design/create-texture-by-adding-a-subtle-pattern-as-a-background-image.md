# [Create Texture by Adding a Subtle Pattern as a Background Image](https://www.freecodecamp.org/learn/responsive-web-design/applied-visual-design/create-texture-by-adding-a-subtle-pattern-as-a-background-image)

---

#### Relevant Links

- [W3Schools - background](https://www.w3schools.com/cssref/css3_pr_background.asp)
- [Mozilla Docs - background](https://developer.mozilla.org/en-US/docs/Web/CSS/background)

---

## Hints

### Hint 1

- Your code should include  `background`  property inside the body selector.

### Hint 2

- You should use the `url()`  function.

### Hint 3

- Ensure the URL passed to the `url` function is: 
`https://cdn-media-1.freecodecamp.org/imgr/MJAkxbh.png`


---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<style>
  body {
    background: url("https://cdn-media-1.freecodecamp.org/imgr/MJAkxbh.png");
  }
</style>
```

</details>
# [Use the flex-basis Property to Set the Initial Size of an Item](https://www.freecodecamp.org/learn/responsive-web-design/css-flexbox/use-the-flex-basis-property-to-set-the-initial-size-of-an-item)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>
You can achieve the same effect as the previous two challenges with `flax-basis`.
After setting sthe appropiate values, you will see `#box-2` being bigger than `#box-1` before any shrinking or grow is applied.

```css
#box-1 {
  background-color: dodgerblue;
  height: 200px;
  flex-basis: 10em;
}

#box-2 {
  background-color: orangered;
  height: 200px;
  flex-basis: 20em;
}
```

</details>
# [Apply the flex-direction Property to Create Rows in the Tweet Embed](https://www.freecodecamp.org/learn/responsive-web-design/css-flexbox/apply-the-flex-direction-property-to-create-rows-in-the-tweet-embed)

---
## Problem Explanation
The purpose of this challenge is to ensure the child elements of <i>header</i> and <i>footer</i> are aligned in a row. While the default flex direction is a row. It is best to be explicit about your styling as much as possible.
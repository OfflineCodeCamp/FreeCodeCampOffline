# [Align Elements Using the align-items Property](https://www.freecodecamp.org/learn/responsive-web-design/css-flexbox/align-elements-using-the-align-items-property)

---
## Problem Explanation
While it is encourage to try the different options for `align-items`, setting is value to `center` is the only way to pass this challenge.

While playing with the different options you will come to realize `strech` is the default value for the property.
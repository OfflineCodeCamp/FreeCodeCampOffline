# [Use the flex-shrink Property to Shrink Items](https://www.freecodecamp.org/learn/responsive-web-design/css-flexbox/use-the-flex-shrink-property-to-shrink-items)

---
## Problem Explanation
When using `flex-shrink` the higher the number the more it shrinks. For this challenge, afte ryou give the proper values, you should see the `#box-1` being wider than `#box-2`.
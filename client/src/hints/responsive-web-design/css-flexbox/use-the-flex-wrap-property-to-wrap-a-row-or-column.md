# [Use the flex-wrap Property to Wrap a Row or Column](https://www.freecodecamp.org/learn/responsive-web-design/css-flexbox/use-the-flex-wrap-property-to-wrap-a-row-or-column)

---
## Problem Explanation
Setting the `flex-wrap` property to `wrap` will make the columns go back to their specified width and the gree and black columns will be wraped to the begining of the frame.

Note that setting it to `wrap-reverse` will not make the grey empty space from the `#box-container` stay at the bottom while changing the columns order.
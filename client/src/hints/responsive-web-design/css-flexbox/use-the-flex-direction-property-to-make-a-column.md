# [Use the flex-direction Property to Make a Column](https://www.freecodecamp.org/learn/responsive-web-design/css-flexbox/use-the-flex-direction-property-to-make-a-column)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

To stack the child elements of your flex container on top of each other you would change the <i>flex-direction</i> the following way:

```CSS
#main-container {
    display: flex;
    flex-direction: column;
}
```
</details>
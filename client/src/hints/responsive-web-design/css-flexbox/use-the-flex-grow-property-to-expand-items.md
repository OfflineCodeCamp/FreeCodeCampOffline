# [Use the flex-grow Property to Expand Items](https://www.freecodecamp.org/learn/responsive-web-design/css-flexbox/use-the-flex-grow-property-to-expand-items)

---
## Problem Explanation
`flex-grow` works similarly to `flex-shrink`, the higher the number, the more it grows.
The `#box-2` box should be wider in this exercise.
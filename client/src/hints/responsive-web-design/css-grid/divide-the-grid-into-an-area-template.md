# [Divide the Grid Into an Area Template](https://www.freecodecamp.org/learn/responsive-web-design/css-grid/divide-the-grid-into-an-area-template)

---
## Problem Explanation
In this challenge you are required to use the "grid-template-areas" property to group cells of a grid together into areas and give the areas a custom name.


---
## Hints

### Hint 1


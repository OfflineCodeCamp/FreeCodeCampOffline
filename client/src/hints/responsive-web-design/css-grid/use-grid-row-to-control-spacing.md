# [Use grid-row to Control Spacing](https://www.freecodecamp.org/learn/responsive-web-design/css-grid/use-grid-row-to-control-spacing)

> MDN: [grid-row](https://developer.mozilla.org/en-US/docs/Web/CSS/grid-row)
> 
> The grid-row CSS property is a [shorthand](https://developer.mozilla.org/en-US/docs/Web/CSS/Shorthand_properties) property for [`grid-row-start`](https://developer.mozilla.org/en-US/docs/Web/CSS/grid-row-start) and [`grid-row-end`](https://developer.mozilla.org/en-US/docs/Web/CSS/grid-row-end) specifying a grid item�s size and location within the grid row by contributing a line, a span, or nothing (automatic) to its grid placement, thereby specifying the inline-start and inline-end edge of its [grid area](https://developer.mozilla.org/en-US/docs/Glossary/grid_areas).


[details="Solution"]
```
<style>
  .item1{background:LightSkyBlue;}
  .item2{background:LightSalmon;}
  .item3{background:PaleTurquoise;}
  .item4{background:LightPink;}

  .item5 {
    background: PaleGreen;
    grid-column: 2 / 4;
    /* Only change code below this line */
    grid-row: 2 / 4;
    /* Only change code above this line */
  }

  .container {
    font-size: 40px;
    min-height: 300px;
    width: 100%;
    background: LightGray;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-template-rows: 1fr 1fr 1fr;
    grid-gap: 10px;
  }
</style>

<div class="container">
  <div class="item1">1</div>
  <div class="item2">2</div>
  <div class="item3">3</div>
  <div class="item4">4</div>
  <div class="item5">5</div>
</div>

```
[/details]
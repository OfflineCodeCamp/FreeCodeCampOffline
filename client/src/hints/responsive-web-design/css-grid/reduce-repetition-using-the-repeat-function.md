# [Reduce Repetition Using the repeat Function](https://www.freecodecamp.org/learn/responsive-web-design/css-grid/reduce-repetition-using-the-repeat-function)
This is a stub. Help the community by [making a suggestion](https://forum.freecodecamp.org/new-topic?category=Contributors&title=&body=**What%20is%20your%20hint%20or%20solution%20suggestion%3F**%0A%0A%0A%0A%0A**Challenge%3A**%20Reduce%20Repetition%20Using%20the%20repeat%20Function%0A%0A%0A**Link%20to%20the%20challenge%3A**%0A%0Ahttps%3A%2F%2Fwww.freecodecamp.org%2Flearn%2Fresponsive-web-design%2Fcss-grid%2Freduce-repetition-using-the-repeat-function) of a hint and/or solution.  We may use your suggestions to update this stub.

---

## Problem Explanation

This summarizes what need to be done without just restating the challenge description and/or instructions. This is an optional section

#### Relevant Links

- [Mozilla Docs: repeat](https://developer.mozilla.org/en-US/docs/Web/CSS/repeat)

---

## Hints

### Hint 1

Hint goes here

### Hint 2

Hint goes here

---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
<style>
  .item1{background:LightSkyBlue;}
  .item2{background:LightSalmon;}
  .item3{background:PaleTurquoise;}
  .item4{background:LightPink;}
  .item5{background:PaleGreen;}

  .container {
    font-size: 40px;
    min-height: 300px;
    width: 100%;
    background: LightGray;
    display: grid;
    /* Only change code below this line */

    grid-template-columns: repeat(3, 1fr);

    /* Only change code above this line */
    grid-template-rows: 1fr 1fr 1fr;
    grid-gap: 10px;
  }
</style>

<div class="container">
  <div class="item1">1</div>
  <div class="item2">2</div>
  <div class="item3">3</div>
  <div class="item4">4</div>
  <div class="item5">5</div>
</div>

```

#### Code Explanation

- Code explanation goes here

#### Relevant Links

- [Link Text](link_url_goes_here)

</details>
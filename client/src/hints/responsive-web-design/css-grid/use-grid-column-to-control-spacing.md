# [Use grid-column to Control Spacing](https://www.freecodecamp.org/learn/responsive-web-design/css-grid/use-grid-column-to-control-spacing)

---

#### Relevant Links

- [Mozilla Docs: grid-column](https://developer.mozilla.org/en-US/docs/Web/CSS/grid-column)

---

## Hints

### Hint 1

Make item 5 consume the last 2 columns in the grid. Currently, the grid extends to column 3. So, start at column 2 and extend past 3.

---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<style>
  .item1{background:LightSkyBlue;}
  .item2{background:LightSalmon;}
  .item3{background:PaleTurquoise;}
  .item4{background:LightPink;}

  .item5 {
    background: PaleGreen;
    /* add your code below this line */
    grid-column: 2 / 4;
    /* add your code above this line */
  }

  .container {
    font-size: 40px;
    min-height: 300px;
    width: 100%;
    background: LightGray;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-template-rows: 1fr 1fr 1fr;
    grid-gap: 10px;
  }
</style>

<div class="container">
  <div class="item1">1</div>
  <div class="item2">2</div>
  <div class="item3">3</div>
  <div class="item4">4</div>
  <div class="item5">5</div>
</div>
```

#### Code Explanation

- Utilizing `2/4` will extend `item5` starting at column 2 and extend through column 3.

#### Relevant Links

- [Mozilla Docs: grid-column](https://developer.mozilla.org/en-US/docs/Web/CSS/grid-column)


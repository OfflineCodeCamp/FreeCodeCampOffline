# [Create Your First CSS Grid](https://www.freecodecamp.org/learn/responsive-web-design/css-grid/create-your-first-css-grid)


---
## Hints

### Hint 1

To change display of any element, the following syntax is used:

```css
display: propertyName;
```

### Hint 2

Change the display property of the .container element

### Hint 3

Set the display of the .container element to grid


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

To set the display of the .container element to grid:

```css
display: grid;
```

</details>
# [Make an Image Responsive](https://www.freecodecamp.org/learn/responsive-web-design/responsive-web-design-principles/make-an-image-responsive)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

Following the instructions:

Add style rules for the img tag to make it responsive to the size of its container. It should display as a block-level element, it should fit the full width of its container without stretching, and it should keep its original aspect ratio.

the style becomes:


# [Create a Media Query](https://www.freecodecamp.org/learn/responsive-web-design/responsive-web-design-principles/create-a-media-query)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

Following the instructions:

Add a media query, so that the p tag has a font-size of 10px when the device's height is less than or equal to 800px.

the media query is:

```css
  /* Add media query below */
  @media (max-height: 800px){ 
    p {
    font-size: 10px;
      }
  }
```
and the size of the text will be 10px when the device's width is less than or equal to 800px.

</details>
# [Comment out HTML](https://www.freecodecamp.org/learn/responsive-web-design/basic-html-and-html5/comment-out-html)

---
## Problem Explanation
Comment syntax is the same of every other HTML element:

Example:
```
<!-- code -->
```
 
 subElement | Description
 ---------- | -----------
 `<!--` | Opening tag
 code | Commented text
 `-->` | Closing tag
 
 If you want to comment just some elements in the code you want to wrap them in a comment element - create an opening tag before them and a closing tag right after.
 
Have fun!
# [Delete HTML Elements](https://www.freecodecamp.org/learn/responsive-web-design/basic-html-and-html5/delete-html-elements)

---
## Problem Explanation
To remove an HTML element you must remove its tag subelements: the content, whatever it is, can be kept since it's not representative of any HTML element.

Remember: if some code other than the part you are asked to modify has been changed you can restart your challenge by clicking the `reset all code` button.

Good luck!
# [Link to External Pages with Anchor Elements](https://www.freecodecamp.org/learn/responsive-web-design/basic-html-and-html5/link-to-external-pages-with-anchor-elements)

---
## Problem Explanation
In this challenge you should use everything you have learned so far.
The HTML elements syntax:
  - Opening tag
  - Content
  - Closing tag

And the tag attributes syntax:
  - `<tag`
  - key="value"
  - `>` ( `/>` in case of self-closing tags )

You are asked to give to an `a` tag the attribute (key) `href` with the value of `"http://freecatphotoapp.com"` and to use the text "cat photos" as content of the same `a` tag 

#### Relevant Links
 - [`<a>` tag - href attribute](https://guide.freecodecamp.org/html/attributes/a-href-attribute)
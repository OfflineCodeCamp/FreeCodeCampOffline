# [Link to Internal Sections of a Page with Anchor Elements](https://www.freecodecamp.org/learn/responsive-web-design/basic-html-and-html5/link-to-internal-sections-of-a-page-with-anchor-elements)

---
## Problem Explanation
As stated in the instructions the internal link is composed of two elements: the `a` tag and the html element with the `id` used in the `href` attribute of the `a` tag.

All of these are valid internal links:
 
ANCHOR TAG | BRINGS TO
----- | ------
`<a href="#destination">I am an internal link</a>` | `<p id="destination">I am the destination of the link</p>`
`<a href="#inlinestuff">I am an internal link</a>` | `<span id="inlinestuff">I am the destination of the link</span>`
`<a href="#title">I am an internal link</a>` | `<h1 id="title">I am the destination of the link</h1>`
`<a href="#mainarticle">I am an internal link</a>` | `<article id="mainarticle">I am the destination of the link</article>`

You are asked to use the existent anchor element to create your internal link, so do not write another anchor tag.

To transform the actual anchor tag is not the only thing the challenge wants you to do though, there are two more points:
 - To remove the `target` attribute of the `a` tag: you don't want anymore to open a new tab in your browser, it's the actual page that will 'move' up/down.
  - To modify the text content of the `a` tag: from `cat photos` to `Jump to Bottom`(double check capitalization).
  
  Good luck!

---


[details="Solution"]
```
<h2>CatPhotoApp</h2>
<main>

  <a href="#footer">Jump to Bottom</a>

  <img src="https://bit.ly/fcc-relaxing-cat" alt="A cute orange cat lying on its back.">

  <p>Kitty ipsum dolor sit amet, shed everywhere shed everywhere stretching attack your ankles chase the red dot, hairball run catnip eat the grass sniff. Purr jump eat the grass rip the couch scratched sunbathe, shed everywhere rip the couch sleep in the sink fluffy fur catnip scratched. Kitty ipsum dolor sit amet, shed everywhere shed everywhere stretching attack your ankles chase the red dot, hairball run catnip eat the grass sniff.</p>
  <p>Purr jump eat the grass rip the couch scratched sunbathe, shed everywhere rip the couch sleep in the sink fluffy fur catnip scratched. Kitty ipsum dolor sit amet, shed everywhere shed everywhere stretching attack your ankles chase the red dot, hairball run catnip eat the grass sniff. Purr jump eat the grass rip the couch scratched sunbathe, shed everywhere rip the couch sleep in the sink fluffy fur catnip scratched.</p>
  <p>Meowwww loved it, hated it, loved it, hated it yet spill litter box, scratch at owner, destroy all furniture, especially couch or lay on arms while you're using the keyboard. Missing until dinner time toy mouse squeak roll over. With tail in the air lounge in doorway. Man running from cops stops to pet cats, goes to jail.</p>
  <p>Intently stare at the same spot poop in the plant pot but kitten is playing with dead mouse. Get video posted to internet for chasing red dot leave fur on owners clothes meow to be let out and mesmerizing birds leave fur on owners clothes or favor packaging over toy so purr for no reason. Meow to be let out play time intently sniff hand run outside as soon as door open yet destroy couch.</p>

</main>

<footer id="footer">Copyright Cat Photo App</footer>
```
[/details]
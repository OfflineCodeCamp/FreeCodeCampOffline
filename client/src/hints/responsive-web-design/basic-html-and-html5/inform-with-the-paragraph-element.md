# [Inform with the Paragraph Element](https://www.freecodecamp.org/learn/responsive-web-design/basic-html-and-html5/inform-with-the-paragraph-element)

---
## Problem Explanation
You are asked to create another HTML element. A brief recap on how to achieve that:
 - Create an opening tag.
 - Write some content (usually plain text)
 - Create a closing tag.
 
 Good luck!
# [Nest Many Elements within a Single div Element](https://www.freecodecamp.org/learn/responsive-web-design/basic-html-and-html5/nest-many-elements-within-a-single-div-element)

---
## Problem Explanation
Using a generic purpose tag as `div` to wrap multiple elements allows the developer to apply a common style to each of the wrapped element, to group elements with the same meaning, to create different layout 'pack' and so on.

About this challenge you have to group the lists already present in the code: you can use the opening div tag just before the paragraph used as header of the unordered list and put the closing tag right below the ordered one.

Good luck!
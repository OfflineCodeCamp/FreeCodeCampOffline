# [Declare the Doctype of an HTML Document](https://www.freecodecamp.org/learn/responsive-web-design/basic-html-and-html5/declare-the-doctype-of-an-html-document)

---
## Problem Explanation
The declaration of the Doctype must be in the first line since it gives instructions to the browser on how to interpret the following lines of code. 

It might be followed by a `<html>` element, even if is not mandatory; this is the root element of the document and if you do not write it most of the browser will infer it. However, it's a good practice to write it explicitly.

All your html elements must be nested inside the `<html>` one: in this challenge you are asked to use a `<h1>` element with content of your choice.

Good luck!
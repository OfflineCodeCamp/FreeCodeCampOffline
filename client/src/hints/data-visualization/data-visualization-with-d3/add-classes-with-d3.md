﻿# [Add Classes with D3](https://www.freecodecamp.org/learn/data-visualization/data-visualization-with-d3/add-classes-with-d3)

---

## Hint

Use the `.attr()` method to add the `class` attribute and set its value to `bar`. The method takes a name and a value. Example, `.attr('id', 'someIdValue')`

---

## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html

<style>
  .bar {
    width: 25px;
    height: 100px;
    display: inline-block;
    background-color: blue;
  }
</style>
<body>
  <script>
    const dataset = [12, 31, 22, 17, 25, 18, 29, 14, 9];

    d3.select("body").selectAll("div")
      .data(dataset)
      .enter()
      .append("div")
      // Add your code below this line

      .attr('class', 'bar')
      
      // Add your code above this line
  </script>
</body>

```
</details>

---

#### Relevant Links

D3 docs: [selection.attr(name[, value])](https://github.com/d3/d3-selection#selection_attr)
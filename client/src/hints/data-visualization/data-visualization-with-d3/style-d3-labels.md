﻿# [Style D3 Labels](https://www.freecodecamp.org/learn/data-visualization/data-visualization-with-d3/style-d3-labels)


---
## Hints

### Hint 1

Chain two ` style() ` method together.

### Hint 2

Use ` font-family ` in the first method and ` fill ` in the second method.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

Add these lines to the end of the chain:

```javascript
  .style("font-size","25px")
  .style("fill","red");
```

</details>
﻿# [Add Axes to a Visualization](https://www.freecodecamp.org/learn/data-visualization/data-visualization-with-d3/add-axes-to-a-visualization)


---
## Hints

### Hint 1

Set the y-axis variable using `const yAxis = d3.axisLeft(yScale);`.

### Hint 2

Append the y-axis using `svg.append()`.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<body>

﻿# [Add a Hover Effect to a D3 Element](https://www.freecodecamp.org/learn/data-visualization/data-visualization-with-d3/add-a-hover-effect-to-a-d3-element)


---
## Hints

### Hint 1

Add the ` bar ` class to all `rect ` elements.

### Hint 2

Use the ` attr() ` method to add attributes.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

Add the following line of code to the end of your ` rect ` methods chain:

```javascript
.attr("class","bar");
```

</details>
﻿# [Use Capture Groups to Search and Replace](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/regular-expressions/use-capture-groups-to-search-and-replace)

---
## Problem Explanation
Using `.replace()` with the first parameter set to find the part of the original string to replace, and the second parameter should be the replacement. 


---
## Hints

### Hint 1


﻿# [Check for All or None](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/regular-expressions/check-for-all-or-none)

---
## Problem Explanation
In this challenge, you are asked to change the regex so that it defines the existence of the letter u, resulting in the acceptance of both American and British versions of the word favourite/favorite. 


---
## Hints

### Hint 1
The variable favWord already contains the American version — favorite. Does the regular expression in favRegex also match the British version?

### Hint 2
Have you used the question mark, ? symbol, if so, have you placed it in the correct position?


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
let favWord = "favorite";
let favRegex = /favou?rite/; // Change this line
let result = favRegex.test(favWord);
```
</details>
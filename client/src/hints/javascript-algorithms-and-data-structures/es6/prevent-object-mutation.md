﻿# [Prevent Object Mutation](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/es6/prevent-object-mutation)


---
## Hints

### Hint 1

*   _Use `Object.freeze()` to prevent mathematical constants from changing._



---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function freezeObj() {

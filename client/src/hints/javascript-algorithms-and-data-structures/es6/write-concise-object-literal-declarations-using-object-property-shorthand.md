﻿# [Write Concise Object Literal Declarations Using Object Property Shorthand](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/es6/write-concise-object-literal-declarations-using-object-property-shorthand)

---
## Problem Explanation
Here, we are tasked at returning an object that accepts the function's parameters as its attributes. 


---
## Hints

### Hint 1

Get rid of the colons, and the duplicate words.



---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
const createPerson = (name, age, gender) => {
  "use strict";
  // change code below this line
  return {
    name,
    age,
    gender
  };
  // change code above this line
};
```
</details>
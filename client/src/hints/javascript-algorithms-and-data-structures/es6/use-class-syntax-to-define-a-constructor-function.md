﻿# [Use class Syntax to Define a Constructor Function](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/es6/use-class-syntax-to-define-a-constructor-function)

---
## Problem Explanation

In this lesson, you are defining the Vegetable object using class syntax.


---
## Hints

### Hint 1

Create the class called `Vegetable`. It will contain the necessary details about the `Vegetable` object.

### Hint 2

Put a constructor with a parameter called `name`, and set it to `this.name`.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript

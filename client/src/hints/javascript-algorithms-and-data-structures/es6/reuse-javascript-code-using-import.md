﻿# [Reuse Javascript Code Using import](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/es6/reuse-javascript-code-using-import)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
import { uppercaseString, lowercaseString } from './string_functions.js';
// add code above this line

uppercaseString("hello");
lowercaseString("WORLD!");
```

</details>
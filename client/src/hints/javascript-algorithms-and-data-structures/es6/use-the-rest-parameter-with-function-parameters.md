﻿# [Use the Rest Parameter with Function Parameters](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/es6/use-the-rest-parameter-with-function-parameters)

#### Relevant LInks

- [Rest parameter explanation](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters "Mozilla Developer Network")

- [Spread operator compared to rest parameter](https://stackoverflow.com/questions/33898512/spread-operator-vs-rest-parameter-in-es2015-es6 "Stack Overflow") 

- Video explaining spread and rest

https://youtu.be/iLx4ma8ZqvQ


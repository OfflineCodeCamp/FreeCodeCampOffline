﻿# [Create a Module Script](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/es6/create-a-module-script)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```html
<html>
  <body>
    <!-- add your code below -->
    <script type="module" src="index.js"></script>
    <!-- add your code above -->
  </body>
</html>
```

</details>
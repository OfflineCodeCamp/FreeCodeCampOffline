﻿# [Set Default Parameters for Your Functions](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/es6/set-default-parameters-for-your-functions)

---
## Problem Explanation

We'll be modifying the increment function so that the **number** parameter is incremented by 1 by default, by setting **value** to 1 if a value for **value** is not passed to the increment function.


---
## Hints

### Hint 1

Let's identify where the parameter **value** is in JS function

try to solve the problem now

### Hint 2

Set **value** equal to something so that it is that value by default

try to solve the problem now



---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript

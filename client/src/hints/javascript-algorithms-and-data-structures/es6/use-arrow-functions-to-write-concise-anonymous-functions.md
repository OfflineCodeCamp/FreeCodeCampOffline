﻿# [Use Arrow Functions to Write Concise Anonymous Functions](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/es6/use-arrow-functions-to-write-concise-anonymous-functions)

---
## Problem Explanation
Again, ES6 is all about making JavaScript more elegant, and for some, more readable. 

Anonymous functions, as stated, can be created when you are sure that the function will not be called by name anywhere else.


---
## Hints

### Hint 1

Get rid of the `function` key word, and plug in this `=>` arrow.

### Hint 2

Did you get rid of the `var` keyword?


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
const magic = () => {

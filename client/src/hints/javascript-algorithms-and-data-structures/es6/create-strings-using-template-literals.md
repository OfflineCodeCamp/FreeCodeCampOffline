﻿# [Create Strings Using Template Literals](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/es6/create-strings-using-template-literals)

---
## Problem Explanation
Instead of using string concatenation, ES6 offers template literals to create strings. In this challenge, you have to use template literals to create an array of text warnings.

It's required to use template literals to return a list as every element in the array as the element will be wrapped in a `<li></li>` tag.


---
## Hints

### Hint 1


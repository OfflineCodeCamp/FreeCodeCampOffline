﻿# [Use Destructuring Assignment to Pass an Object as a Function's Parameters](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/es6/use-destructuring-assignment-to-pass-an-object-as-a-functions-parameters)

---
## Problem Explanation

You could pass the entire object, and then pick the specific attributes you want by using the `.` operator. But ES6 offers a more elegant option!


---
## Hints

### Hint 1

Get rid of the `stats`, and see if you can destructure it. We need the `max` and `min` of `stats`.



---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
const stats = {
  max: 56.78,
  standard_deviation: 4.34,
  median: 34.54,
  mode: 23.87,
  min: -0.75,
  average: 35.85
};


# [Combine Arrays with the Spread Operator](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-data-structures/combine-arrays-with-the-spread-operator)


---
## Solutions

<details><summary>Solution 2 (Click to Show/Hide)</summary>

```javascript
function spreadOut() {
  let fragment = ["to", "code"];
  let sentence = ["learning", ...fragment, "is", "fun"]; // change this line
  return sentence;
}

// do not change code below this line
console.log(spreadOut());
```
#### Code Explanation
- The solution is exactly like the example given. Simply insert the `fragment[]` array into the `sentence[]` array at the desired index.
</details>
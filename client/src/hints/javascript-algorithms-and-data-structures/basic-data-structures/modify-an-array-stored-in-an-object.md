# [Modify an Array Stored in an Object](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-data-structures/modify-an-array-stored-in-an-object)

---
## Problem Explanation

- The function can be written in just two lines of code.
- The first line should just use the `push()` function to add the `friend`parameter to the array found in `user.data.friend`. The second line will return the modified array.

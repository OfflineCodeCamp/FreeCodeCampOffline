﻿# [Palindrome Checker](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/javascript-algorithms-and-data-structures-projects/palindrome-checker)

---
Hints/Solutions are not provided for certification projects as these are intended to be a test of your learning and understanding.
# [Escape Sequences in Strings](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/escape-sequences-in-strings)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
var myStr = "FirstLine\n\t\\SecondLine\nThirdLine";
```
</details>
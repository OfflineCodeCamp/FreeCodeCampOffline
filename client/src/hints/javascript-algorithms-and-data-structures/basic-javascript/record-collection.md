﻿# [Record Collection](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/record-collection)

---
## Problem Explanation

For the given `id` parameter, which is associated with the `records` object:
    *   If the `value` parameter isn't an empty string, update (or set) the `value` parameter for the `prop` parameter.
    *   If the `prop` parameter is equal to `"tracks"` and the `value` isn't an empty string, push the `value` onto the end of the `tracks` array.
    *   If `value` is an empty string, delete that `prop` from the object.
*   Finally, return `records`.

---
## Hints

### Hint 1

Use an `if...else if` chain to check the needed steps.

### Hint 2

To access the value of a key in this object, you will use `records[id][prop]`.

### Hint 3

You can't `push` to an array that doesn't exist. Use `hasOwnProperty` to check first.


---
## Solutions

[details="Solution 1 (Click to Show/Hide)"]

```javascript
// Setup
var recordCollection = {
  2548: {
    albumTitle: 'Slippery When Wet',
    artist: 'Bon Jovi',
    tracks: ['Let It Rock', 'You Give Love a Bad Name']
  },
  2468: {
    albumTitle: '1999',
    artist: 'Prince',
    tracks: ['1999', 'Little Red Corvette']
  },
  1245: {
    artist: 'Robert Palmer',
    tracks: []
  },
  5439: {
    albumTitle: 'ABBA Gold'
  }
};

// Only change code below this line
function updateRecords(records, id, prop, value) {
  if (prop !== 'tracks' && value !== "") {
    records[id][prop] = value;
  } else if (prop === "tracks" && records[id].hasOwnProperty("tracks") === false) {
    records[id][prop] = [value];
  } else if (prop === "tracks" && value !== "") {
    records[id][prop].push(value);
  } else if (value === "") {
    delete records[id][prop];
  }
  return records;
}

updateRecords(recordCollection, 5439, 'artist', 'ABBA');
```

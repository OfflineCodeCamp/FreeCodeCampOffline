﻿# [Return a Value from a Function with Return](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/return-a-value-from-a-function-with-return)

---
## Problem Explanation
Using `return`, you can make functions output data. 


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function timesFive(num) {
  return num * 5;
}
```
</details>
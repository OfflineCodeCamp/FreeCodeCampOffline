﻿# [Use Bracket Notation to Find the First Character in a String](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/use-bracket-notation-to-find-the-first-character-in-a-string)


---
## Hints

### Hint 1
Remember that the first character of a string is at the zero-th position. For example:

```javascript
var str = "Hello";
var letter = str[0]; // This equals "H"
```
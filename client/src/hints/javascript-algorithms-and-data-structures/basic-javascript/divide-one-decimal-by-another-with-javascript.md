# [Divide One Decimal by Another with JavaScript](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/divide-one-decimal-by-another-with-javascript)


---
## Hints

### Hint 1
JavaScript uses the `/` symbol for division.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
var quotient = 4.4 / 2.0; // Fix this line
```
</details>
﻿# [Initializing Variables with the Assignment Operator](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/initializing-variables-with-the-assignment-operator)


---
## Hints

### Hint 1
To <strong>initialize</strong> a variable is to give it an initial value. You can declare and initialize a variable like so:

```javascript
var num = 5; // This is equal to 5
```
﻿# [Use Recursion to Create a Countdown](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/use-recursion-to-create-a-countdown)

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function countdown(n) {
  if (n < 1) {
    return [];
  } else {
    const arr = countdown(n - 1);
    arr.unshift(n);
    return arr;
  }
}
```
</details>

<details><summary>Solution 2 (Click to Show/Hide)</summary>

```javascript

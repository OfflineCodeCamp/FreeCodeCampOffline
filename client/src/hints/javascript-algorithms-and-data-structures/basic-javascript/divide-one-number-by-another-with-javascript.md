# [Divide One Number by Another with JavaScript](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/divide-one-number-by-another-with-javascript)


---
## Hints

### Hint 1
JavaScript uses the `/` symbol for division.

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
var quotient = 6 / 3; //quotient will get value 2
```
</details>
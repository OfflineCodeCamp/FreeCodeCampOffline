﻿# [Local Scope and Functions](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/local-scope-and-functions)

---
## Problem Explanation
Local scope means that the variable is available within a certain area. In the case of this exercise, `myVar` is only available within the function, and not anywhere outside. 


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript

# [Comparisons with the Logical Or Operator](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/comparisons-with-the-logical-or-operator)



---
## Solutions
<details><summary>Solution 1 (Click to Show/Hide)</summary>


```javascript
if (val < 10 || val > 20) {
  return "Outside";
}
```

#### Code Explanation
* The above code will return "Outside" only if `val` is between 10 and 20 (inclusive).
</details>
﻿# [Passing Values to Functions with Arguments](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/passing-values-to-functions-with-arguments)

---
## Problem Explanation

Our task is to create a function that has **parameters**. These are inputs that determine the function's output. You place paramaters inside the `()`, like so:

```javascript
function functionWithArgs(one, two) {
  console.log(one + two);
}
```
We now have to add code inside the brackets. Our task is to add `one` and `two`, and print the sum to the console. 

```javascript
functionWithArgs(7, 3);
//This will console log 10.
```
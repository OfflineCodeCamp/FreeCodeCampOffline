# [Finding a Remainder in JavaScript](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/finding-a-remainder-in-javascript)


---
## Hints

### Hint 1
The remainder operator `%` gives the remainder of the division of two numbers.

```javascript
var remainder = 11 % 3; //remainder gets the value 2
```
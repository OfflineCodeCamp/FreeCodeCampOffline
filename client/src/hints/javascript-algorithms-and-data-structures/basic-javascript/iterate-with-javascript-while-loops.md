﻿# [Iterate with JavaScript While Loops](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/iterate-with-javascript-while-loops)

---
## Problem Explanation
While loops will run as long as the condition inside the ( ) is true.

Example:

```javascript
while (condition) {
  //code...
}
```

---
## Hints

### Hint 1
Use a iterator variable such as i in your condition
```javascript

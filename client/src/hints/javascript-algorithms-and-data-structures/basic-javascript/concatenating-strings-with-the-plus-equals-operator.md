# [Concatenating Strings with the Plus Equals Operator](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/concatenating-strings-with-the-plus-equals-operator)


---
## Hints

### Hint 1
The '+=' operator can concatenate (link) strings easily. Make sure your spelling is right, and you've left appropriate spaces.

```javascript
var str = "Hello ";
str += "coding"; // Now the string reads "Hello coding"
str += "camper!"; // And now the string reads "Hello codingcamper!"
```
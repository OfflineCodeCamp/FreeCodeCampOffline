# [Compound Assignment With Augmented Addition](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/compound-assignment-with-augmented-addition)


---
## Hints

### Hint 1
Computers read from left to right. So, using the '+=' operator means that the variable is added with the right number, then the variable is assigned to the sum. Like so:
```js
var a = 9;
a += 10; // Now, 'a' is equal to 19
```
﻿# [Nest one Array within Another Array](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/nest-one-array-within-another-array)


---
## Hints

### Hint 1
Arrays are one-dimensional; that means they store only one row of data. But you can make an array multi-dimensional by putting arrays inside arrays! Like so:

```javascript
var arr = [["Two-dimensional", 2], ["Two rows", 12]];
```

The above array has two dimensions.
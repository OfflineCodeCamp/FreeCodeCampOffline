# [Create Decimal Numbers with JavaScript](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/create-decimal-numbers-with-javascript)


---
## Hints

### Hint 1
JavaScript number variables can have decimals.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
var myDecimal = 2.8;
```
</details>
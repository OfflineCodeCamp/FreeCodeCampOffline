﻿# [Understanding Uninitialized Variables](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/understanding-uninitialized-variables)


---
## Hints

### Hint 1
Make sure that the variable has the correct data value. Leaving a variable uninitialized, meaning you do not give it a value, can cause problems if you want to do operations on it.
# [Assignment with a Returned Value](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/assignment-with-a-returned-value)


---
## Hints

### Hint 1
Functions act as placeholders for the data they output. Basically, you can assign the output of a function to a variable, just like any normal data.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
processed = processArg(7); // Equal to 2
```
</details>
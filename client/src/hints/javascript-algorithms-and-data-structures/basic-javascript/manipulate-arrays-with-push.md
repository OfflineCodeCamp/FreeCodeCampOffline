﻿# [Manipulate Arrays With push()](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/manipulate-arrays-with-push)


---
## Hints

### Hint 1
The `push()` method allows you to append (add to the end) an element to an array. Like so...

```javascript
var arr = [1, 2, 3, 4];
arr.push(5); // Now, the array is [1, 2, 3, 4, 5]
```
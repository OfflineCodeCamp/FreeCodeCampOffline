﻿# [Multiply Two Numbers with JavaScript](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/multiply-two-numbers-with-javascript)


---
## Hints

### Hint 1
JavaScript uses the `*` symbol for multiplication.

```javascript
var product = 8 * 10; //product gets the value 80
```
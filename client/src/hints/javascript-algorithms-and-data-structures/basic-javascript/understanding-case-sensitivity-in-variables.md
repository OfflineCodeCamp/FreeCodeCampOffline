﻿# [Understanding Case Sensitivity in Variables](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/understanding-case-sensitivity-in-variables)

---
## Problem Explanation

A popular programming norm is to use <strong>Camel case</strong> when creating variable names. Notice the first word is lowercase and every following word is uppercase. Here are some examples:

```javascript
var camelCase;
var someNumber;
var theBestVariableEver;
var weWillStoreNumbersInThisVariable;
```
# [Add Two Numbers with JavaScript](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/add-two-numbers-with-javascript)


---
## Hints

### Hint 1
JavaScript uses the `+` symbol for addition.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
var sum = 10 + 10; //sum gets the value 20
```
</details>
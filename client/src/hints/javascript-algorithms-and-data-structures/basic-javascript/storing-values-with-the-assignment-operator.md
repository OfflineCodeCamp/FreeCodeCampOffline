﻿# [Storing Values with the Assignment Operator](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/storing-values-with-the-assignment-operator)


---
## Hints

### Hint 1
It's just like math! You can set a variable to act as a placeholder for data by using the '=' assignment operator. In other words, the variable will store the data.

```javascript
var a;
a = 5; // The variable 'a' is equal to 5


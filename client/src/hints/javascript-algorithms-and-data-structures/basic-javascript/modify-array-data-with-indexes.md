﻿# [Modify Array Data With Indexes](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/modify-array-data-with-indexes)


---
## Hints

### Hint 1
Access the position of the array, and change it, like so:

```javascript
var arr = [1, 2, 3, 4, 5];

arr[0] = 6; // Now, the array is [6, 2, 3, 4, 5]
arr[4] = 10; // Now, the array is [6, 2, 3, 4, 10]
```

The important concept here is that arrays are **mutable**. This means that they can be changed easily.
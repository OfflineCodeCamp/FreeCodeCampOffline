# [Declare String Variables](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/declare-string-variables)


---
## Hints

### Hint 1
In JavaScript, variables are <strong>dynamic</strong>. That means they can hold numbers, strings, or any other data type at a given time. To declare a string, simply <strong>initialize</strong> (create) a variable:

```javascript
var a;
```
    
Then, using single or double quotation marks, declare the string:

```javascript
a = "Hello Camper!";
```
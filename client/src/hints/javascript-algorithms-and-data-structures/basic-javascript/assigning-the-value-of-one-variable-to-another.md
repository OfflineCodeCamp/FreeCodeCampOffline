# [Assigning the Value of One Variable to Another](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/assigning-the-value-of-one-variable-to-another)


---
## Hints

### Hint 1
Variables can be assigned a value from a another variable. The value of the assigned variable will be the same value as the variable that was used to assign it.

```javascript
var a;
a = 5; // The variable 'a' is equal to 5

var abc;
abc = a; // The variable 'abc' is equal to 5
```
﻿# [Profile Lookup](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/profile-lookup)

---
## Problem Explanation

*   Change the code below `// Only change code below this line` and up to `// Only change code above this line`.
*   Ensure that you are editing the inside of the `lookUpProfile()` function.
    *   This function includes two parameters, **firstName** and **prop**.
*   The function should look through the **contacts** list for the given **firstName** parameter.
    *   If there is a match found, the function should then look for the given **prop** parameter.
    *   If both **firstName** and the associated **prop** are found, you should return the value of the **prop**.
    *   If **firstName** is found and no associated **prop** is found, you should return `No such property`.
*   If **firstName** isn't found anywhere, you should return `No such contact`.


---
## Hints

### Hint 1

Use a `for` loop to cycle through the **contacts** list.

### Hint 2

Use a nested `if` statement to first check if the **firstName** matches, and then checks `if` the **prop** matches.

### Hint 3

Leave your `return "No such contact"` out of the `for` loop as a final catch-all.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript    
function lookUpProfile(name, prop) {

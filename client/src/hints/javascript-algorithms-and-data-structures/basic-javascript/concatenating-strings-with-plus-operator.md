# [Concatenating Strings with Plus Operator](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/concatenating-strings-with-plus-operator)


---
## Hints

### Hint 1
Concatenate means to link together. Think of the '+' operator like a chain linking strings together; add the strings just like you add numbers. Make sure your spelling is correct! Take note of spaces between words.

```javascript
var str = "Good " + "job!"; // It says "Good job!"
var abc = "Good" + "job!"; // It says "Goodjob!"
```
# [Comparison with the Greater Than Or Equal To Operator](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/comparison-with-the-greater-than-or-equal-to-operator)

* `>=` (Greater Than or Equal To) is a logical operator that returns true case the value on the left is the **same or higher** than the one on the right.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function testGreaterOrEqual(val) {
  if (val >= 20) return "20 or Over";

  if (val >= 10) return "10 or Over";

  return "Less than 10";
}
```
</details>
﻿# [Subtract One Number from Another with JavaScript](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/subtract-one-number-from-another-with-javascript)


---
## Hints

### Hint 1
Using the '-' subtraction operator, you can get the difference of two numbers...

```javascript
var diff1 = 30 - 14; // Difference is 16
var diff2 = 90 - 60; // Difference is 30
```
# [Decrement a Number with JavaScript](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/decrement-a-number-with-javascript)


---
## Hints

### Hint 1
Decrement a number using the '--' operator:

```javascript
var a = 5;
a--; // Now, 'a' is 4
--a; // Now, 'a' is 3
```
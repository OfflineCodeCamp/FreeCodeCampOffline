﻿# [Updating Object Properties](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/updating-object-properties)


---
## Hints

### Hint 1
Use dot ** . ** notation to access the object property.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
// Setup
var myDog = {
  name: "Coder",
  legs: 4,
  tails: 1,
  friends: ["freeCodeCamp Campers"]
};

// Only change code below this line.

myDog.name = "Happy Coder"; // Solution
```

</details>
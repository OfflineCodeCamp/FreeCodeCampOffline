# [Add New Properties to a JavaScript Object](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/add-new-properties-to-a-javascript-object)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
var myDog = {
  name: "Happy Coder",
  legs: 4,
  tails: 1,
  friends: ["freeCodeCamp Campers"]
};

// Only change code below this line.
myDog.bark = "woof";
```
</details>
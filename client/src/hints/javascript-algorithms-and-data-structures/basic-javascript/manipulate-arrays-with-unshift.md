﻿# [Manipulate Arrays With unshift()](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/manipulate-arrays-with-unshift)


---
## Hints

### Hint 1
While `push()` added elements to the back of the array, `unshift()` adds them to the front. All you have to do is:

```javascript
var arr = [2, 3, 4, 5];
arr.unshift(1); // Now, the array has 1 in the front
```
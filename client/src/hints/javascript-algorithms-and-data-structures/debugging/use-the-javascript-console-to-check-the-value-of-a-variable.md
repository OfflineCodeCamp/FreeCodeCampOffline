﻿# [Use the JavaScript Console to Check the Value of a Variable](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/debugging/use-the-javascript-console-to-check-the-value-of-a-variable)

<details><summary>Solution 1 (Click to Show/Hide)</summary>


---
## Solutions

```js
let a = 5;
let b = 1;
a++;
// Add your code below this line

let sumAB = a + b;
console.log(sumAB);

console.log(a);
```

</details>
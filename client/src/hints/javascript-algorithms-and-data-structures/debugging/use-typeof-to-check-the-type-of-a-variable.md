﻿# [Use typeof to Check the Type of a Variable](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/debugging/use-typeof-to-check-the-type-of-a-variable)


---
## Hints

### Hint 1
Use `console.log(typeof variable)` to display the type of the desired variable.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
console.log(typeof seven);
console.log(typeof three);
```

</details>
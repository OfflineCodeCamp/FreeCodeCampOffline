﻿# [Catch Mixed Usage of Single and Double Quotes](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/debugging/catch-mixed-usage-of-single-and-double-quotes)


---
## Hints

### Hint 1
Remember whether you choose to use single or double quotes, simply adding  a `\` before the character will allow the character to fit in the string without closing either single or double quotes. 


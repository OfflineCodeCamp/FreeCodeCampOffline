﻿# [Pass Arguments to Avoid External Dependence in a Function](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/functional-programming/pass-arguments-to-avoid-external-dependence-in-a-function)


---
## Hints

### Hint 1

Try to pass argument to function and return increased value of this argument. 



---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
// the global variable
var fixedValue = 4;

// Add your code below this line
function incrementer(value) {
  return value + 1;

  // Add your code above this line
}


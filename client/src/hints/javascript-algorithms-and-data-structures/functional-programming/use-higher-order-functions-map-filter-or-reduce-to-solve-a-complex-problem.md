﻿# [Use Higher-Order Functions map, filter, or reduce to Solve a Complex Problem](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/functional-programming/use-higher-order-functions-map-filter-or-reduce-to-solve-a-complex-problem)

---
## Problem Explanation

We need to compute and square values from the `realNumberArray` and store them in the variable `squaredIntegers` using the `map()`, `filter()`, and/or `reduce()` functions.


---
## Hints

### Hint 1

*   You will need to `filter()` the `realNumberArray` for positive integers (decimals are not integers).


### Hint 2

*   You will need to `map()` the values from your `filter()` function to the variable `squaredIntegers`.


### Hint 3

*   Remember the magic of chaining functions.

---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js

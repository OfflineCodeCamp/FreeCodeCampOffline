﻿# [Refactor Global Variables Out of Functions](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/functional-programming/refactor-global-variables-out-of-functions)


---
## Hints

### Hint 1
- If you're having trouble with changing bookList, try using a copy of the array in your functions. 

### Hint 2
- Here's some more information about [how JavaScript handles function arguments](https://codeburst.io/javascript-passing-by-value-vs-reference-explained-in-plain-english-8d00fd06a47c).


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript

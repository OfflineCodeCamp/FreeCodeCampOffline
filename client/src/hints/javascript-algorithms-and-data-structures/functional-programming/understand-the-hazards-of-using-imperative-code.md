﻿# [Understand the Hazards of Using Imperative Code](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/functional-programming/understand-the-hazards-of-using-imperative-code)


---
## Problem Explanation

What you should notice is the fact that output is not as suggested in instructions, which should be the following array:

﻿# [Spinal Tap Case](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting/spinal-tap-case)

---
## Problem Explanation

Convert the given string to a lowercase sentence with words joined by dashes.

#### Relevant Links

*   <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String' target='_blank' rel='nofollow'>String global object</a>
*   <a href=' https://forum.freecodecamp.org/t/a-quick-and-simple-guide-to-javascript-regular-expressions/190263' target='_blank' rel='nofollow'>JS Regex Resources</a>
*   <a href='http://forum.freecodecamp.com/t/javascript-string-prototype-replace/15942' target='_blank' rel='nofollow'>JS String Prototype Replace</a>
*   <a href='http://forum.freecodecamp.com/t/javascript-string-prototype-tolowercase/15948' target='_blank' rel='nofollow'>JS String Prototype ToLowerCase</a>


---
## Hints

### Hint 1

Create a regular expression for all white spaces and underscores.

### Hint 2

You will also have to make everything lowercase.

### Hint 3


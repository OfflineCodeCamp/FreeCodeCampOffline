﻿# [Sum All Odd Fibonacci Numbers](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting/sum-all-odd-fibonacci-numbers)

---
## Problem Explanation

You will need to gather all the **Fibonacci** numbers and then check for the odd ones. Once you get the odd ones then you will add them all. The last number should be the number given as a parameter if it actually happens to be an off Fibonacci number.

#### Relevant Links

*   <a href='https://en.wikipedia.org/wiki/Fibonacci_number' target='_blank' rel='nofollow'>Fibonacci number</a>


---
## Hints

### Hint 1

To get the next number of the series, you need to add the current one to the previous and that will give you the next one.

### Hint 2

To check if a number is even all you have to check is if `number % 2 == 0`.

### Hint 3

As you get the next odd one, don't forget to add it to a global variable that can be returned at the end. `result += currNumber;` will do the trick.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function sumFibs(num) {

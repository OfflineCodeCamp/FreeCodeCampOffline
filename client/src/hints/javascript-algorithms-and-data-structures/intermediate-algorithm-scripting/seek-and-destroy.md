﻿# [Seek and Destroy](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting/seek-and-destroy)

---
## Problem Explanation

This problem is a bit tricky because you have to familiarize yourself with Arguments, as you will have to work with two **or more** but on the script you only see two. You will remove any number from the first argument that is the same as any other other arguments.

#### Relevant Links

*   <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments' target='_blank' rel='nofollow'>Arguments object</a>
*   <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter' target='_blank' rel='nofollow'>Array.filter()</a>


---
## Hints

### Hint 1

You need to work with `arguments` as if it was a regular array. The best way is to convert it into one.

### Hint 2

You may want to use use various methods like: `indexOf()`, `includes()`, or `filter()`. When in doubt about any function, check those docs!

---
## Solutions
<details><summary>Solution 1 (Click to Show/Hide)</summary>


```javascript
function destroyer(arr) {

﻿# [Map the Debris](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting/map-the-debris)

---
## Problem Explanation

The first thing to do is to get familiar with what the program is for by knowing what Orbital period exactly is. You've to return a new array that transforms the element's average altitude into their orbital periods. The parts generally found hard are finding the formula, implementing it and for some people, modifying objects by the key. However, something that is not very clear is the fact that your program has to be able to check for any number of objects in the array; This is what is tested on the second part.

#### Relevant Links

*   <a href='https://en.wikipedia.org/wiki/Orbital_period' target='_blank' rel='nofollow'>Orbital period</a>
*   <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object' target='_blank' rel='nofollow'>JS Objects</a>
*   <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/PI' target='_blank' rel='nofollow'>Math.PI</a>
*   <a href='http://forum.freecodecamp.com/t/javascript-math-pow/14685' target='_blank' rel='nofollow'>JS Math Pow</a>
*   <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/sqrt' target='_blank' rel='nofollow'>Math.sqrt()</a>
*   <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round' target='_blank' rel='nofollow'>Math.round()</a>


---
## Hints

### Hint 1


﻿# [Sum All Primes](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting/sum-all-primes)

---
## Problem Explanation

For this problem will find all prime numbers up to the number you are given as a parameter and return their sum. It is a good idea to research algorithms for finding prime numbers.

#### Relevant Links

*   [Prime numbers](https://en.wikipedia.org/wiki/Prime_number)


---
## Hints

### Hint 1

You can use the definition of [prime numbers](https://en.wikipedia.org/wiki/Prime_number) or try learning and implementing your own [Sieve of Eratosthenes](https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes). Check this [link](https://stackoverflow.com/questions/11966520/how-to-find-prime-numbers-between-0-100) to see a StackOverflow discussion on various ways of finding prime numbers.

### Hint 2

This problem can be hard if you create your own code to check for primes, so don't feel bad if you use someone's algorithm for that part. Research is an important part of coding!

---
## Solutions

[details="Solution 1 - Divisibility checking (Click to Show/Hide)"]
```
function sumPrimes(num) {
  // Helper function to check primality
  function isPrime(num) {
    for (let i = 2; i <= Math.sqrt(num); i++) {
      if (num % i == 0)
        return false;
    }
    return true;
  }

  // Check all numbers for primality
  let sum = 0;
  for (let i = 2; i <= num; i++) {
    if (isPrime(i))
      sum += i;
  }
  return sum;
}
```
**Code Explanation**
We loop over all values in our range, adding them to the sum if they are prime.
Our primality checking function returns `false` if the target number is divisible by any number in between 2 and the square root of the target number. We only need to check up to the square root because the square root of a number is the largest possible unique divisor.
[/details]


[details="Solution 2 - List of prime numbers (Click to Show/Hide)"]
```
function sumPrimes(num) {
  // Check all numbers for primality
  let primes = [];
  for (let i = 2; i <= num; i++) {
    if (primes.every((prime) => i % prime !== 0))
      primes.push(i);
  }
  return primes.reduce((sum, prime) => sum + prime, 0);
}
```
**Code Explanation**
This solution is very similar to Solution 1.
In this solution we retain a list of all primes found so far and check if any of these numbers divide into each number in our range.

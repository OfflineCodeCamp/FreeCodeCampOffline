﻿# [Sum All Numbers in a Range](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting/sum-all-numbers-in-a-range)

---
## Problem Explanation

You need to create a program that will take an array of two numbers who are not necessarily in order, and then add not just those numbers but any numbers in between. For example, [3,1] will be the same as `1+2+3` and not just `3+1`


---
## Hints

### Hint 1

Use `Math.max()` to find the maximum value of two numbers.

### Hint 2

Use `Math.min()` to find the minimum value of two numbers.

### Hint 3

Remember to that you must add all the numbers in between so this would require a way to get those numbers.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function sumAll(arr) {

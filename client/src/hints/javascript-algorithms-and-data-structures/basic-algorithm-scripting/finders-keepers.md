# [Finders Keepers](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-algorithm-scripting/finders-keepers)

---
## Problem Explanation
We need to return the element from an array that passes a function. Both the `function` and the `array` are passed into our function `findElement(arr, func)`.



---
## Hints

### Hint 1
We need to return the element from an array that passes a function. Both the `function` and the `array` are passed into our function `findElement(arr, func)`. Looking through the array can be done with a `for` loop.

### Hint 2
`num` is passed to the function. We will need to set it to the elements we want to check with the function.

### Hint 3
Do not forget, if none of the numbers in the array pass the test, it should return `undefined`.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function findElement(arr, func) {
  let num = 0;


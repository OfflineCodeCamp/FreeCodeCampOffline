# [Falsy Bouncer](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-algorithm-scripting/falsy-bouncer)

---
## Problem Explanation

Remove all <a href='https://guide.freecodecamp.org/javascript/falsy-values/' target='_blank' rel='nofollow'>falsy</a> values from an array.

#### Relevant Links

*   <a href='https://guide.freecodecamp.org/javascript/falsy-values/' target='_blank' rel='nofollow'>Falsy Values</a>

---
## Hints

### Hint 1

Falsy is something which evaluates to FALSE. There are only six falsy values in JavaScript: undefined, null, NaN, 0, "" (empty string), and false of course.

### Hint 2

We need to make sure we have all the falsy values to compare, we can know it, maybe with a function with all the falsy values...


### Hint 3

Then we need to add a `filter()` with the falsy values function...


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
function bouncer(arr) {
  let newArray = [];

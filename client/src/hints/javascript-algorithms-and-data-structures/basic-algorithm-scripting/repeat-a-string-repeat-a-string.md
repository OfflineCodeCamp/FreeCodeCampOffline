# [Repeat a String Repeat a String](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-algorithm-scripting/repeat-a-string-repeat-a-string)

---
## Problem Explanation

The program is very simple, we have to take a variable and return that variable being repeated certain amount of times. No need to add space or anything, just keep repeating it into one single string.

#### Relevant Links

*   <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String' target='_blank' rel='nofollow'>Global String Object</a>


---
## Hints

### Hint 1
The program is very simple, we have to take a variable and return that variable being repeated certain amount of times. No need to add space or anything, just keep repeating it into one single string.

### Hint 2

You can't edit strings, you will need to create a variable to store the new string.


### Hint 3

Create a loop to repeat the code as many times as needed.


### Hint 4

Make the variable created store the current value and append the word to it.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
function repeatStringNumTimes(str, num) {
  var accumulatedStr = "";

  while (num > 0) {
    accumulatedStr += str;
    num--;
  }

  return accumulatedStr;
}
```

#### Code Explanation

*   Create an empty string variable to store the repeated word.
*   Use a while loop or for loop to repeat code as many times as needed according to `num`
*   Then we just have to add the string to the variable created on step one, and increase or decrease `num` depending on how you set the loop.
*   At the end of the loop, return the variable for the repeated word.

#### Relevant Links

*   <a>JS while Loop</a>
*   <a href='https://forum.freecodecamp.com/t/javascript-for-loop/14666s-Explained' target='_blank' rel='nofollow'>JS For Loops Explained</a>
</details>

<details><summary>Solution 2 (Click to Show/Hide)</summary>

```js
function repeatStringNumTimes(str, num) {
  if (num < 1) {
    return "";

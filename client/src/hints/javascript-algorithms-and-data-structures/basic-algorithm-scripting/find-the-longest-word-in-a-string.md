# [Find the Longest Word in a String](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-algorithm-scripting/find-the-longest-word-in-a-string)

---
## Problem Explanation

You have to go through each word and figure out which one is the longest and return the length of that word.

#### Relevant Links

*   <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/length' target='_blank' rel='nofollow'>JS String Length</a>


---
## Hints

### Hint 1

You will need to loop through the words in the string.


### Hint 2

You will need to figure out a way to keep track globally of the greatest current length.


### Hint 3

Do you remember how to get the length of strings?


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function findLongestWordLength(str) {
  let words = str.split(' ');
  let maxLength = 0;

  for (let i = 0; i < words.length; i++) {
    if (words[i].length > maxLength) {
      maxLength = words[i].length;
    }
  }

  return maxLength;
}
```

#### Code Explanation

Take the string and convert it into an array of words. Declare a variable to keep track of the maximum length and loop from 0 to the length of the array of words.

Then check for the longest word by comparing the current word to the previous one and storing the new longest word. At the end of the loop just return the number value of the variable maxLength.

#### Relevant Links

*   <a href='https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/length' target='_blank' rel='nofollow'>JS Array.length</a>
</details>

<details><summary>Solution 2 (Click to Show/Hide)</summary>

**Using `.reduce()`**

    function findLongestWordLength(s) {
      return s.split(' ')
        .reduce(function(longest, word) {
          return Math.max(longest, word.length)
        }, 0);
    }


#### Code Explanation

For more information on `reduce` <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce' target='_blank' rel='nofollow'>click here.</a>  

In case you're wondering about that `0` after the callback function, it is used to give an initial value to the `longest`, so that `Math.max` will know where to start.

#### Relevant Links

*   <a href='https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/functional-programming/use-the-reduce-method-to-analyze-data' target='_blank' rel='nofollow'>JS Reduce</a>
*   <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/max' target='_blank' rel='nofollow'>JS Math Max</a>

</details>

<details><summary>Solution 3 (Click to Show/Hide)</summary>

**Using `.map()`**

```javascript
function findLongestWordLength(str) {
  return Math.max(...str.split(" ").map(word => word.length));
}
```

#### Code Explanation

We provide `Math.max` with the length of each word as argument, and it will simply return the highest of all. 

Let's analyze everything inside the `Math.max` parenthesees to understand how we do that. 

`str.split(" ")` splits the string into an array, taking spaces as separators. It returns this array: \["The","quick,"brown","fox","jumped","over","the","lazy","dog"\].

Then, we will make another array, made from the lengths of each element of the `str.split(" ")` array with `map()`.

`str.split(" ").map(word => word.length)` returns \[3, 5, 5, 3, 6, 4, 3, 4, 3\]

Finally, we pass the array as argument for the Math.max function with the spread operator `...`

For more information on `map` <a href='https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/map' target='_blank' rel='nofollow'>click here.</a>
</details>

<details><summary>Solution 4 (Click to Show/Hide)</summary>

```
function findLongestWordLength(str) {
  // split the string into individual words
  const words = str.split(" ");

  // words only has 1 element left that is the longest element
  if (words.length == 1) {
    return words[0].length;
  }

  // if words has multiple elements, remove the first element
  // and recursively call the function
  return Math.max(
    words[0].length,
    findLongestWordLength(words.slice(1).join(" "))
  );
}

findLongestWordLength("The quick brown fox jumped over the lazy dog");
```

#### Code Explanation

The first line splits the string into individual words. Then we check if  `words` only has 1 element left. If so, then that is the longest element and we return it. Otherwise, we remove the first element and recursively call the function  `findLongestWord`, returning the maximum between the length of the first result and the recursive call .

#### Relevant Links

* [JS Functions](https://www.youtube.com/watch?v=R8SjM4DKK80)
* [Recursion Basics](https://www.youtube.com/watch?v=k7-N8R0-KY4)

</details>
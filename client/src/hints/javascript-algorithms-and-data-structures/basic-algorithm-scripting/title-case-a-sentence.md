# [Title Case a Sentence](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-algorithm-scripting/title-case-a-sentence)

---
## Problem Explanation

We have to return a sentence with title case. This means that the first letter will always be in uppercase and the rest will be in lowercase.

#### Relevant Links

*   <a href='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String' target='_blank' rel='nofollow'>Global String Object</a>
*   <a href='http://forum.freecodecamp.com/t/javascript-string-prototype-tolowercase/15948' target='_blank' rel='nofollow'>JS String Prototype ToLowerCase</a>
*   <a href='http://forum.freecodecamp.com/t/javascript-string-prototype-touppercase/15950' target='_blank' rel='nofollow'>JS String Prototype ToUpperCase</a>
*   <a href='http://forum.freecodecamp.com/t/javascript-string-prototype-replace/15942' target='_blank' rel='nofollow'>JS String Prototype Replace</a>


---
## Hints

### Hint 1

*   You should start by splitting the string into an array of words.
*   Split the sentence.


### Hint 2

*   You should make the word lowercase before making the first letter uppercase.
*   Use replace method on each word to capitalize the first letter of each word.


### Hint 3

*   You will need to create a new string with pieces of the previous one and at the end merge everything into a single string again.
*   In replace method, give first argument as the position of the first letter using charAt. For second argument write a function to return the capitalized letter as the replacement.


---
## Solutions 

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```js
String.prototype.replaceAt = function(index, character) {
  return (
    this.substr(0, index) + character + this.substr(index + character.length)
  );
};

function titleCase(str) {
  var newTitle = str.split(" ");
  var updatedTitle = [];
  for (var st in newTitle) {
    updatedTitle[st] = newTitle[st]
      .toLowerCase()
      .replaceAt(0, newTitle[st].charAt(0).toUpperCase());
  }
  return updatedTitle.join(" ");
}
```

#### Code Explanation

We are modifying the `replaceAt` function using prototype to facilitate the use of the program.

Split the string by white spaces, and create a variable to track the updated title. Then we use a loop to turn turn the first character of the word to uppercase and the rest to lowercase. by creating concatenated string composed of the whole word in lowercase with the first character replaced by its uppercase.

#### Relevant Links


# [Mutations](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-algorithm-scripting/mutations)

---
## Problem Explanation

*   Return true if the string in the first element of the array contains all of the letters of the string in the second element of the array..

#### Relevant Links

*   <a href='http://forum.freecodecamp.org/t/javascript-string-prototype-indexof-index-of-explained-with-examples/15936' target='_blank' rel='nofollow'>String.indexOf()</a>


---
## Hints

### Hint 1

*   If everything is lowercase it will be easier to compare.


### Hint 2

*   Our strings might be easier to work with if they were arrays of characters.


### Hint 3

*   A loop might help. Use `indexOf()` to check if the letter of the second word is on the first.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

**Procedural**

```js
function mutation(arr) {

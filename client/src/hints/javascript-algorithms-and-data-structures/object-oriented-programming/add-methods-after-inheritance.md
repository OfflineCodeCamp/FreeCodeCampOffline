﻿# [Add Methods After Inheritance](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/object-oriented-programming/add-methods-after-inheritance)

---
## Problem Explanation

Just like the following example, a new instance of an object - `Dog` - must be created and the `prototype` must be set. 

```javascript
function Bird() {}
Bird.prototype = Object.create(Animal.prototype);
Bird.prototype.constructor = Bird;
```

Then a new function - `bark()` - must be added to the Dog prototype. 


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function Animal() {}
Animal.prototype.eat = function() {
  console.log("nom nom nom");
};

function Dog() {}

// Add your code below this line
Dog.prototype = Object.create(Animal.prototype);
Dog.prototype.constructor = Dog;
Dog.prototype.bark = function() {

﻿# [Understand Own Properties](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/object-oriented-programming/understand-own-properties)

---
## Problem Explanation

In the example code given you will see a new array `ownProps[]` intialised followed by a `for...in` statement to loop through the properties of `duck` and then use a `push()` statement to fill in the new array. The same method must be followed for the `canary` object. 

Simply replace the `duck` object in the 'for...in' statement with the `canary`object to pass all test cases.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript

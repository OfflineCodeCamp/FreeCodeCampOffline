﻿# [Use a Mixin to Add Common Behavior Between Unrelated Objects](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/object-oriented-programming/use-a-mixin-to-add-common-behavior-between-unrelated-objects)

---
## Problem Explanation

Just like the `flyMixin` function, a new `glideMixin` function must be made to accept both `bird` and `boat` objects as a parameter. Create this new function using the same syntax as the `flyMixin` function and then call the function on both objects. 


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
let bird = {
  name: "Donald",
  numLegs: 2
};

let boat = {
  name: "Warrior",
  type: "race-boat"
};

// Add your code below this line
let glideMixin = function(obj) {
  obj.glide = function() {
    console.log("Gliding!");
  };
};
glideMixin(bird);
glideMixin(boat);
```
</details>
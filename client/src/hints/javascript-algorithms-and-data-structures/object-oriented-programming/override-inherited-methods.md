﻿# [Override Inherited Methods](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/object-oriented-programming/override-inherited-methods)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
Penguin.prototype.fly = function() {
  return "Alas, this is a flightless bird.";
};
```

</details>
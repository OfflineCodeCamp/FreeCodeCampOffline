﻿# [Use Inheritance So You Don't Repeat Yourself](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/object-oriented-programming/use-inheritance-so-you-dont-repeat-yourself)


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function Cat(name) {
  this.name = name;
}

Cat.prototype = {
  constructor: Cat
};

function Bear(name) {
  this.name = name;
}

Bear.prototype = {
  constructor: Bear
};

function Animal() {}

Animal.prototype = {
  constructor: Animal,
  eat: function() {
    console.log("nom nom nom");
  }
};
```

#### Code Explanation
* Remove the "eat" method from Cat.prototype and Bear.prototype and add it to the Animal.prototype.
</details>
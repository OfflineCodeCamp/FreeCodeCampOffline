﻿# [Create a Basic JavaScript Object](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/object-oriented-programming/create-a-basic-javascript-object)

---
## Problem Explanation

The soultion to this problem is more or less identical to the example given.
Give the `dog` object two new properties - `name` and `numLegs` -  and set them to a string and a number, respectively.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
let dog = {
  name: "George",
  numLegs: 4
};
```

</details>
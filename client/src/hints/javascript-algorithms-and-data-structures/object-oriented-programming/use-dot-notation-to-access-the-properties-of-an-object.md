﻿# [Use Dot Notation to Access the Properties of an Object](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/object-oriented-programming/use-dot-notation-to-access-the-properties-of-an-object)

---
## Problem Explanation

The following code will simply print `1` from the `obj` object.

```javascript
let obj = {
  property1: 1,
  property2: 2
};

console.log(obj.property1);
```

Following this logic, use the `console.log` operation to print both `property1`and `property2`to the screen.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
let dog = {
  name: "Spot",
  numLegs: 4
};
// Add your code below this line
console.log(dog.name);
console.log(dog.numLegs);
```

</details>
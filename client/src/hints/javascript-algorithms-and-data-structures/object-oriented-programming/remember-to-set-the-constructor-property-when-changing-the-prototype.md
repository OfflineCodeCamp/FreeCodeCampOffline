﻿# [Remember to Set the Constructor Property when Changing the Prototype](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/object-oriented-programming/remember-to-set-the-constructor-property-when-changing-the-prototype)


---
## Hints

### Hint 1
Remember to define the constructor property when you set a prototype to a new object.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
Dog.prototype = {
  constructor: Dog, // Solution

  numLegs: 2,
  eat: function() {
    console.log("nom nom nom");
  },
  describe: function() {
    console.log("My name is " + this.name);
  }
};
```

</details>
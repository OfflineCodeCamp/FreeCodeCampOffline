﻿# [Define a Constructor Function](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/object-oriented-programming/define-a-constructor-function)

## MProblem Explanation

The `Dog()` function should be written in the exact same format as the `Bird()` function given in the example. Simply replace `Bird` with `Dog` to pass all test cases.


---
## Solutions

<details><summary>Solution 1 (Click to Show/Hide)</summary>

```javascript
function Dog() {
  (this.name = "Geogre"), (this.color = "White"), (this.numLegs = 4);
}
```

</details>
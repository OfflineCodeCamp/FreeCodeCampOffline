import React from "react";
import FreeCodeCampLogo from "../../../assets/icons/FreeCodeCamp-logo";

const NavLogo = (): JSX.Element => {
    return <h3 className="nav-title">Free Code Camp Offline</h3>;
};

NavLogo.displayName = "NavLogo";
export default NavLogo;

/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable react/prop-types */
// @ts-nocheck
import React, { Ref } from "react";
import { isChallenge, isLocationSuperBlock } from "../../../utils/path-parsers";
import { Link } from "../../helpers";
import NavLogo from "./nav-logo";
import "./universal-nav.css";
import { navigate } from "gatsby";
import { useLocation } from "@reach/router";
import { useTranslation } from "react-i18next";
import HomeLogo from "../../../assets/icons/home-logo";
import FreeCodeCampLogo from "../../../assets/icons/FreeCodeCamp-logo";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export interface UniversalNavProps {
    displayMenu?: boolean;
    fetchState?: { pending: boolean };
    menuButtonRef?: Ref<HTMLButtonElement> | undefined;
    searchBarRef?: unknown;
    toggleDisplayMenu?: React.MouseEventHandler<HTMLButtonElement> | undefined;
    user?: Record<string, unknown>;
}
export const UniversalNav = ({
    displayMenu
}: UniversalNavProps): JSX.Element => {
    const { t } = useTranslation();
    const location = useLocation();
    const isPageChallenge = isChallenge(location.pathname);
    const isCoursePage = isLocationSuperBlock(location);

    const handleGoBackLesson = () => {
        const pathSplitted = location.pathname.split("/");
        const lessonPageUrl = `/learn/${pathSplitted[2]}/#${pathSplitted[3]}`;
        navigate(lessonPageUrl);
    };

    return (
        <nav
            className={"universal-nav" + (displayMenu ? " expand-nav" : "")}
            id="universal-nav"
        >
            <div
                className={
                    "universal-nav-left" +
                    (displayMenu ? " display-search" : "")
                }
            >
                {/* {search} */}
                {isPageChallenge && (
                    <div
                        style={{
                            display: "flex",
                            alignItems: "center",
                            cursor: "pointer"
                        }}
                        onClick={handleGoBackLesson}
                    >
                        {/* {t("buttons.go-back-to-lessons")} */}
                        <div className="universal-nav-go-back">
                            <FontAwesomeIcon icon={faAngleLeft} size={60} />
                        </div>
                    </div>
                )}
                {isCoursePage && (
                    <div
                        style={{
                            display: "flex",
                            alignItems: "center",
                            cursor: "pointer"
                        }}
                        onClick={() => navigate("/learn")}
                    >
                        {/* {t("buttons.go-back-to-curriculum")} */}
                        <div className="universal-nav-go-back">
                            <FontAwesomeIcon icon={faAngleLeft} size={60} />
                        </div>
                    </div>
                )}
            </div>
            <div className="universal-nav-middle">
                <Link id="universal-nav-logo" to="/">
                    {/* <div>Free Code Camp Offline</div> */}
                    <div class="nav-title">Free Code Camp Offline</div>
                </Link>
            </div>
            <div className="universal-nav-right main-nav nav-placeholder">
                <Link id="home-nav-logo" to="/">
                    <HomeLogo />
                </Link>
            </div>
        </nav>
    );
};

UniversalNav.displayName = "UniversalNav";
export default UniversalNav;

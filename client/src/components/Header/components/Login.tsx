import { Button } from "@freecodecamp/react-bootstrap";
import React from "react";
import { useTranslation } from "react-i18next";
import { connect } from "react-redux";
import { createSelector } from "reselect";
import { Link } from "../../helpers";

import envData from "../../../../../config/env.json";
import { isSignedInSelector } from "../../../redux";

import "./login.css";

const { apiLocation, homeLocation } = envData;

const mapStateToProps = createSelector(isSignedInSelector, (isSignedIn) => ({
    isSignedIn
}));

export interface LoginProps {
    block?: boolean;
    children?: unknown;
    "data-test-label"?: string;
    isSignedIn?: boolean;
    isLanding?: boolean;
}

const Login = (): JSX.Element => {
    const { t } = useTranslation();

    return (
        <Link noUnderLine to="/learn">
            <span className="btn-cta-big btn-block signup-btn btn-cta btn btn-default">
                {t("buttons.get-start-btn")}
            </span>
        </Link>
    );
};

Login.displayName = "Login";

export default connect(mapStateToProps)(Login);

import React from 'react';
import HomeLogo from '../../../assets/icons/home-logo';

const NavLogo = (): JSX.Element => {
  return <HomeLogo />;
};

NavLogo.displayName = 'NavLogo';
export default NavLogo;

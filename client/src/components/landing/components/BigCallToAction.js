import PropTypes from "prop-types";
import React from "react";
import Login from "../../Header/components/Login";

const propTypes = {
    page: PropTypes.string
};

const BigCallToAction = () => {
    return <Login/>;
};

BigCallToAction.displayName = "BigCallToAction";
BigCallToAction.propTypes = propTypes;
export default BigCallToAction;

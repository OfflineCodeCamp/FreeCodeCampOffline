import { Col, Row } from "@freecodecamp/react-bootstrap";
import PropTypes from "prop-types";
import React from "react";
import { useTranslation } from "react-i18next";
import { Spacer } from "../../helpers";
import BigCallToAction from "./BigCallToAction";

const propTypes = {
    page: PropTypes.string
};

function LandingTop({ page }) {
    const { t } = useTranslation();

    return (
        <div className="landing-top">
            <Row>
                <Spacer />
                <Col lg={8} lgOffset={2} sm={10} smOffset={1} xs={12}>
                    <h1
                        className="big-heading"
                        data-test-label={`${page}-header`}
                    >
                        {t("landing.big-heading-1")}
                    </h1>
                    <Spacer />
                    <BigCallToAction/>
                    <Spacer />
                </Col>
            </Row>
        </div>
    );
}

LandingTop.displayName = "LandingTop";
LandingTop.propTypes = propTypes;
export default LandingTop;

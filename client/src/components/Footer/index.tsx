import React from "react";
import { useTranslation } from "react-i18next";
import Link from "../helpers/link";
import "./footer.css";

type ColHeaderProps = {
    children: string | React.ReactNode | React.ReactElement;
};

// TODO: Figure out what ColHeader does: 'ColHeader' is declared but its value is never read.
// eslint-disable-next-line
const ColHeader = ({ children, ...other }: ColHeaderProps): JSX.Element => (
    <div className="col-header" {...other}>
        {children}
    </div>
);

function Footer(): JSX.Element {
    const { t } = useTranslation();

    return (
        <footer className="site-footer">
            <div className="footer-container">
                <div className="footer-top">
                    <div className="footer-desc-col">
                        <p>
                            Free Code Camp Offline is an independent fork of the
                            Free Code Camp project
                        </p>
                        <p>
                            Our goal is to bring Free Code Camps excellent
                            curriculum to users that may not have reliable
                            internet.
                        </p>
                        <div>
                            <p>Find out more on our Gitlab project page at: </p>

                            <p style={{ wordBreak: "break-all" }}>
                                <a href="https://gitlab.com/OfflineCodeCamp/FreeCodeCampOffline">
                                    https://gitlab.com/OfflineCodeCamp/FreeCodeCampOffline
                                </a>
                            </p>
                        </div>

                        {/* <div style={{ display: "flex", gap: "5px" }}>
                            <p>Find out more on our Gitlab project page at </p>
                            <a href="https://gitlab.com/OfflineCodeCamp/FreeCodeCampOffline">
                                https://gitlab.com/OfflineCodeCamp/FreeCodeCampOffline
                            </a>
                        </div> */}
                    </div>
                </div>
                <div className="footer-bottom">
                    <div className="col-header">
                        {t("footer.our-nonprofit")}
                    </div>
                    <div className="footer-divder" />
                    <div className="our-nonprofit">
                        <Link external={false} to={t("links:footer.about-url")}>
                            {t("footer.links.about")}
                        </Link>
                        <Link
                            external={false}
                            to={
                                "https://gitlab.com/OfflineCodeCamp/FreeCodeCampOffline"
                            }
                        >
                            {t("footer.links.open-source")}
                        </Link>
                        <Link
                            external={false}
                            to={
                                "https://gitlab.com/OfflineCodeCamp/FreeCodeCampOffline/-/issues"
                            }
                        >
                            {t("footer.links.support")}
                        </Link>
                    </div>
                </div>
            </div>
        </footer>
    );
}

Footer.displayName = "Footer";
export default Footer;

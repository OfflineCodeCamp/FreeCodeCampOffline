import { Modal } from "@freecodecamp/react-bootstrap";
import React from "react";
import { withTranslation } from "react-i18next";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { useStaticQuery, graphql } from "gatsby";
import { useLocation } from "@reach/router";
import { closeModal, isHintModalOpenSelector } from "../redux";

import "./hint-modal.css";
import Hint from "./Hint";

const mapStateToProps = (state) => ({ isOpen: isHintModalOpenSelector(state) });
const mapDispatchToProps = (dispatch) =>
    bindActionCreators({ closeHintModal: () => closeModal("hint") }, dispatch);

function HintModal({ isOpen, closeHintModal }) {
    const { t } = useTranslation();
    const location = useLocation();
    const slug = location.pathname.split("/learn")[1] + "/";
    const data = useStaticQuery(graphql`
        {
            allMarkdownRemark(
                filter: { fields: { slug: { regex: "/^(?!/learn/).+$/" } } }
            ) {
                nodes {
                    fields {
                        slug
                    }
                    html
                }
            }
        }
    `);
    const hintData = data.allMarkdownRemark.nodes.find(
        (node) => node.fields.slug === slug
    );

    return (
        <Modal dialogClassName="hint-modal" onHide={closeHintModal} show={isOpen}>
            <Modal.Header
                className="hint-modal-header fcc-modal"
                closeButton={true}
            >
                <Modal.Title className="text-center">
                    {t("buttons.get-hint-title")}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className="hint-modal-body">
                <Hint hintData={hintData}></Hint>
            </Modal.Body>
        </Modal>
    );
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withTranslation()(HintModal));

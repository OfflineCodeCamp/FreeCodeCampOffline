import React, { useRef, useEffect } from "react";
import "./hint.css";
import { defineCustomElements as deckDeckGoHighlightElement } from "@deckdeckgo/highlight-code/dist/loader";
deckDeckGoHighlightElement();

export default function Hint({ hintData }) {
    const hintRef = useRef();
    useEffect(() => {
        try {
            const h1Element = hintRef.current.children[0];
            h1Element.innerHTML = h1Element.innerText;
        } catch (e) {
            console.log("a tag is not found!");
        }
    }, []);

    return (
        <div className="hint">
            <div
                ref={hintRef}
                dangerouslySetInnerHTML={{ __html: hintData?.html }}
            ></div>
        </div>
    );
}

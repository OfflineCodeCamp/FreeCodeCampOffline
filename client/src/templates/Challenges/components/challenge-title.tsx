import i18next from 'i18next';
import React from 'react';
import GreenPass from '../../../assets/icons/green-pass';
import { Link } from '../../../components/helpers/index';
import BreadCrumb from './bread-crumb';

import './challenge-title.css';

interface ChallengeTitleProps {
  block: string;
  children: string;
  isCompleted: boolean;
  superBlock: string;
  translationPending: boolean;
}

function ChallengeTitle({
  block,
  children,
  isCompleted,
  superBlock,
  translationPending
}: ChallengeTitleProps): JSX.Element {
  return (
    <div className='challenge-title-wrap'>
      <BreadCrumb block={block} superBlock={superBlock} />
      <div className='challenge-title'>
        <div className='title-text'>
          <b>{children}</b>
          {isCompleted ? (
            <GreenPass
              style={{ height: '15px', width: '15px', marginLeft: '7px' }}
            />
          ) : null}
        </div>
      </div>
    </div>
  );
}

ChallengeTitle.displayName = 'ChallengeTitle';

export default ChallengeTitle;

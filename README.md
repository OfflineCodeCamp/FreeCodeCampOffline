
![Header Image](/uploads/b732f49c9dcc67bbc7b0a3b18ddea6d3/image.png)
# Offline Free Code Camp

![Build Passing](https://gitlab.com/OfflineCodeCamp/FreeCodeCampOffline/badges/main/pipeline.svg)

This project is an experimental fork of Free Code Camp with the goal of creating an offline optimized application for users without reliable internet connections. It is not an official project from Free Code Camp and all support issues should be directed to this project.
## Introduction

Although many of us take our internet connections for granted, it may not always be the case for someone wanting to learn to code. This project seeks to make coding education more accessible for those users.

We've taking the curriculum from Free Code Camp and wrapped it in an Election application. Users can simple download this application and either run it on their local machine, or place it on a USB drive and run it on any machine they have access to.

## High level goals for the beta version

- The download is a reasonable size. FCC's curriculum can be quite large, but it's text heavy and should compress well.
- The application should work well on moderately old Windows machines. Users may not have the latest and greatest hardware.
- Users should have access to the bulk of the coding curriculum under FCC (some curriculum is heavily video based and therefore not a good fit)

## Download

Downloads can be found in the [Releases tab on this project](https://gitlab.com/OfflineCodeCamp/FreeCodeCampOffline/-/releases)

This project is currently only available for Windows and Linux. OSX support coming soon.

## Development

## Run client on local machine

1- Clone the repo

```bash
git clone --dept=1 https://github.com/mdp/FreeCodeCampOffline.git
```

2-  Setup the environment variable file

```bash
# Create a copy of the "sample.env" and name it ".env".
# Populate it with the necessary API keys and secrets:

# macOS / Linux
cp sample.env .env

# Windows
copy sample.env .env
```

3- Install dependencies

```bash
npm ci
```

4- Start freeCodeCamp client application

```bash
cd client
npm run develop
# You can now view @freecodecamp/client in the browser.
# http://localhost:8000/
```

5- To run on different languages

```bash
# Chinese
CLIENT_LOCALE=chinese CURRICULUM_LOCALE=chinese npm run develop
```



## Build client

Change working directory to client.

```bash
cd client
```

Run following command to build client. It will build and move output to electron/langs folder. If you don't give any language option, it will build English by default.

```bash
# English (Default)
APP_LANG=en npm run electron:build 

# Espanol
CLIENT_LOCALE=espanol CURRICULUM_LOCALE=espanol APP_LANG=es npm run electron:build
```

### Available languages to build application

| Language            | CLIENT_LOCALE       | CURRICULUM_LOCALE   | APP_LANG |
| ------------------- | ------------------- | ------------------- | -------- |
| English             | english             | english             | en       |
| Spanish             | espanol             | espanol             | es       |
| Chinese             | chinese             | chinese             | zh       |
| Chinese Traditional | chinese-traditional | chinese-traditional | zh-Hant  |
| Italian             | italian             | italian             | it       |
| Portuguese          | portuguese          | portuguese          | pt-BR    |



## Run build with Electron

Change working directory to electron. 

```bash
cd electron
```

After made a build we will have static output of client. We can use it to run application with Electron. To do that we can run following command. Also if you build multiple languages you will have them on langs folder. Electron reads `APP_LANG` environment variable to run application with that language.

```bash
npm run electron:run

# Espanol
APP_LANG=es npm run electron:run
```

## Package build with Electron

Change working directory to electron. 

```bash
cd electron
```

After run application successfully we can package it now for different platforms. To do that we just need to run following command. 

```bash
# Package for Windows
npm run electron:pack-win

# Package for Linux
npm run electron:pack-linux

# Package for Windows
npm run electron:pack-mac

# Package for all platforms
npm run electron:pack-all
```

## Build and package Client

Change working directory to electron. 

```bash
cd electron
```

You don't need to run build and package separately. You can run following command to build and package client with specific language. It will package for windows by default. You can change `electron:build-and-pack` command to package for other platforms.

```bash
# Build and package for Italian
CLIENT_LOCALE=italian CURRICULUM_LOCALE=italian APP_LANG=it npm run electron:build-and-pack
```

## Update curriculum with FCC

```bash
cd client
npm run update-curriculum
```



## License

Licensed under the same license as Free Code Camp, [BSD 3 Clause License](./LICENSE)
